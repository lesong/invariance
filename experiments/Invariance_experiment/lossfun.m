function [L,g] = lossfun(alpha1,K,K1,K2,y,g)
b=alpha1(1);
alpha=alpha1(2:end);

select=(alpha'*K1+b)'.*y;
L=sum(max(0,1-select))+0.5*alpha'*K*alpha ...
    +0.5*alpha'*K2*alpha;

if nargout>1
   
 indicator=(select<1).*(-y);
  g(1,:)=sum(indicator);
  
  g(2:end,:)=K1*(indicator)+ (K*alpha) + K2*alpha;
end

end

