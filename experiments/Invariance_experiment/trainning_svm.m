function [ alpha ] = trainning_svm( a0,K,y_train,lambda,options)

 K=lambda*K;
 
 
 len=length(a0);
 g=zeros(len,1);
 

 alpha=minFunc(@svmloss,a0,options,K,y_train,g);

end
