
clear all;
close all; 

%%generate data

  dataOptions.dim = 50;   
  dataOptions.signal = 10;  
  dataOptions.nsample = 100;
  dataOptions.ntrainningdata = 20;
  dataOptions.noise = 0.5;
  
[ x_train, y_train, x_test, y_test ] = generatedata( dataOptions );  


%tuning parameters
fprintf(1, '--finding kernel bandwidth\n'); 
sigma= 0.4*median(pdist([x_train;x_test])); %kernel bandwidth

l = 4;

theta = randi( dataOptions.dim-1,1,l); %translation parameter


lambda = 10; %regularization parameter for invariance constraint
tau = 1e-1; %regularization parameter for |f|

% generate kernels
fprintf(1, '--generating kernel matrices\n'); 

 K=rbf_kernel(x_train,x_train,sigma);
 K_test=rbf_kernel(x_train,x_test,sigma);
 dK=first_derivative_kernel( x_test, x_train, theta, sigma);
 dK_test=first_derivative_kernel( x_test, x_test, theta, sigma);
 d2K = second_derivative_kernel( x_test, theta, sigma); 
%  dK=first_derivative_kernel( x_train, x_train, theta, sigma, 1);
%  dK_test=first_derivative_kernel( x_train, x_test, theta, sigma, 1);
%  
%  d2K = second_derivative_kernel( x_train, x_train, theta, sigma,1);


 % Optimazation
 ntr=size(K,1);
 nl=size(d2K,1);
 a0=zeros(ntr+nl+1,1);


options.Method='lbfgs';% Method - [ sd | csd | bb | cg | scg | pcg | {lbfgs} | newton0 | pnewton0 |
                        %       qnewton | mnewton | newton | tensor ]
options.MaxIter=500; % MaxIter - Maximum number of iterations allowed (500)
options.MaxFunEvals=1000; % MaxFunEvals - Maximum number of function evaluations allowed (1000)
options.Display='off'; %   Display - Level of display [ off | final | (iter) | full | excessive ]

fprintf(1, '--running optimization\n');  
tic
alpha_inv  = trainning_inv( a0,K,dK,d2K,y_train,lambda,tau,options);
toc

ftest_inv = predict(alpha_inv,[K_test;dK_test]);
ftrain_inv = predict(alpha_inv,[K;dK]);


train_error=1 - mean(max(sign(y_train.*ftrain_inv'), 0));
fprintf('---train error with invariance constraint: %f\n', train_error) 
test_error=1 - mean(max(sign(y_test.*ftest_inv'), 0));
fprintf('---test error with invariance constraint: %f\n', test_error) 



% virtual samples
fprintf(1, '--generating virtual examples to put in the loss function\n'); 
[ntr,d]=size(x_train);
x_virtual=zeros(l*ntr,d);
for i=1:l
x_virtual([(i-1)*ntr+1:i*ntr],:)=x_train(:,[theta(i)+1:end , 1:theta(i)]);
end
y_virtual=repmat(y_train,l,1);


lambda0=tau;

K_virtual=rbf_kernel(x_virtual,x_virtual,sigma);
K_test_v=rbf_kernel(x_virtual,x_test,sigma);

a0=zeros(size(K_virtual,1)+1,1);

options.Method='lbfgs';
options.MaxIter=1000;
options.MaxFunEvals=1000;

fprintf(1, '--running the optimization\n'); 
alpha_virtual  = trainning_svm( a0,K_virtual,y_virtual,lambda0,options);

K_test_v=rbf_kernel(x_virtual,x_test,sigma);

ftest_virtual=predict(alpha_virtual,K_test_v);
ftrain_virtual=predict(alpha_virtual,K_virtual);

train_error_v=1 - mean(max(sign(y_virtual.*ftrain_virtual'), 0));
fprintf('---train error with virtual examples: %f\n', train_error_v) 
test_error_v=1 - mean(max(sign(y_test.*ftest_virtual'), 0));
fprintf('---test error with virtual examples: %f\n', test_error_v) 




fprintf(1,'--virtual example for training data put in loss function and the test data for invariance regularization\n');
% generate kernels
fprintf(1, '--generating kernel matrices\n'); 


dK=first_derivative_kernel( x_test, x_virtual, theta, sigma);

 % Optimazation


 
a0=zeros(size(K_virtual,1)+nl+1,1);
fprintf(1, '--running optimization\n'); 
tic
alpha_virtual_inv  = trainning_inv( a0,K_virtual,dK,d2K,y_virtual,lambda,tau,options);
toc
ftest_virtual_inv = predict(alpha_virtual_inv ,[K_test_v;dK_test]);
ftrain_virtual_inv = predict(alpha_virtual_inv ,[K_virtual;dK]);

train_error=1 - mean(max(sign(y_virtual.*ftrain_virtual_inv'), 0));
fprintf('---train error with invariance constraint and virtual examples: %f\n', train_error) 
test_error=1 - mean(max(sign(y_test.*ftest_virtual_inv'), 0));
fprintf('---test error with invariance constraint and virtual examples: %f\n', test_error) 
