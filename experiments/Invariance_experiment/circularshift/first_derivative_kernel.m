function [ dK ] = first_derivative_kernel( x1, x2, theta, sigma)
%  partial derivative with respect to x1(theta);

[n1,d]=size(x1);
n2=size(x2,1);

%partial derivative of x1 with respect to theta;
l=length(theta);
x1_theta=zeros(l*n1,d);

for i=1:l
x1_theta([(i-1)*n1+1:i*n1],:)=x1(:,[theta(i)+1:end , 1:theta(i)]);
end
dtheta=diff([x1_theta,x1_theta(:,1)],1,2);

%partial derivative of f with respect to x1(theta);
Krbf=rbf_kernel(x1_theta,x2,sigma);
dx=bsxfun(@minus, (dtheta*x2'),sum(x1_theta.*dtheta,2));

dK=(dx./sigma^2).*Krbf;


end