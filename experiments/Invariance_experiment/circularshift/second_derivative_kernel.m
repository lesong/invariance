function [ d2K ] = second_derivative_kernel( x, theta, sigma)

[n,d]=size(x);
l=length(theta);
ntheta=n*l;

%partial derivative of x with respect to theta;

x_theta=zeros(ntheta,d);

for i=1:l
x_theta([(i-1)*n+1:i*n],:)=x(:,[theta(i)+1:end , 1:theta(i)]);

end

dtheta=diff([x1_theta,x1_theta(:,1)],1,2);


Krbf=rbf_kernel(x_theta,x_theta,sigma);

B=bsxfun(@minus, (dtheta*x_theta'),sum(x_theta.*dtheta,2));

B=B .* B'+sigma^2*(dtheta*dtheta');
d2K = Krbf .* B ./ (sigma.^4);


end