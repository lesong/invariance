function [ x_train, y_train, x_test, y_test ] = generatedata( o )
  d = o.dim;   
  s = o.signal;  
  N = o.nsample;
  ntr=o.ntrainningdata;
  noise=o.noise;
X0=[1:s,zeros(1,d-s);[fix(s/2)+1:s,1:fix(s/2)],zeros(1,d-s)];

idx=unidrnd(d,1,N);

X=zeros(N,d);

for i = 1: N
      XX=X0(mod(i,2)+1,:);   
      X(i,:)=XX([idx(i)+1:end , 1:idx(i)])+normrnd(0,noise,1,d);
end
Y=repmat([1;-1],N/2,1);

 x_train=X(1:ntr,:);
 y_train=Y(1:ntr);
 x_test=X(ntr+1:end,:);
 y_test=Y(ntr+1:end,:);
 
p=randperm(ntr);
x_train=x_train(p,:);

y_train=y_train(p);

end

