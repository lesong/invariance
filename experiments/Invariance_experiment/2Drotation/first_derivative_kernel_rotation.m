function [ dK ] = first_derivative_kernel_rotation( x1, x2, theta, sigma,k)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
[n1,d]=size(x1);
n2=size(x2,1);
l=length(theta);
R=reshape([cos(theta);-sin(theta);sin(theta);cos(theta)],d,d*l);
Rk=reshape([cos(theta+k);-sin(theta+k);sin(theta+k);cos(theta+k)],d,d*l);
Rkm=reshape([cos(theta-k);-sin(theta-k);sin(theta-k);cos(theta-k)],d,d*l);

x1_theta=reshape((x1*R)',d,n1*l)';
x1_thetak=reshape((x1*Rk)',d,n1*l)';
x1_thetam=reshape((x1*Rkm)',d,n1*l)';

dtheta=(x1_thetak-x1_thetam)/(2*k);



Krbf=rbf_kernel(x1_theta,x2,sigma);

dx=bsxfun(@minus, (dtheta*x2'),sum(x1_theta.*dtheta,2));

dK=(dx./sigma^2).*Krbf;

end

