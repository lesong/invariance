function [ x_train, y_train, x_test, y_test ] = generateRotation( o )
  
  N = o.nsample;
  ntr=o.ntrainningdata;
  noise=o.noise;
  circle=2;
  diff=o.diff;


X=zeros(N,2);
theta=rand(1,N)*2*pi;

for i = 1: N
    R=[cos(theta(i)),-sin(theta(i));sin(theta(i)),cos(theta(i))];
        X(i,:)=[diff*mod(i,circle)+1,diff*mod(i,circle)+1]*R'+normrnd(0,noise,1,2);
    if mod(i,circle) <floor(circle/2)
        Y(i,:)=1;
    else
        Y(i,:)=-1;
        
    end
end

 x_train=X(1:ntr,:);
 y_train=Y(1:ntr);
 x_test=X(ntr+1:end,:);
 y_test=Y(ntr+1:end,:);
 
p=randperm(ntr);
x_train=x_train(p,:);

y_train=y_train(p);

end
