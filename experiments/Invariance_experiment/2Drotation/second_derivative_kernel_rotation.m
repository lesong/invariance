function [ d2K ] = second_derivative_kernel_rotation( x, theta, sigma,k)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[n,d]=size(x);
l=length(theta);


ntheta=n*l;


R=reshape([cos(theta);-sin(theta);sin(theta);cos(theta)],d,d*l);
Rk=reshape([cos(theta+k);-sin(theta+k);sin(theta+k);cos(theta+k)],d,d*l);
Rkm=reshape([cos(theta-k);-sin(theta-k);sin(theta-k);cos(theta-k)],d,d*l);

x_theta=reshape((x*R)',d,ntheta)';
x_k=reshape((x*Rk)',d,ntheta)';
x_kminus=reshape((x*Rkm)',d,ntheta)';


dtheta=(x_k-x_kminus)/(2*k);



Krbf=rbf_kernel(x_theta,x_theta,sigma);

B=bsxfun(@minus, (dtheta*x_theta'),sum(x_theta.*dtheta,2));

B=B .* B'+sigma^2*(dtheta*dtheta');
d2K = Krbf .* B ./ (sigma.^4);

end

