function plotClassifier(x1,y1,x_train,y_train,x_test,w,sigma,theta,k,method)
%x1 virtual samples

figure;
[n,p] = size(x_train);


hold on

        plot(x1(y1==1,1),x1(y1==1,2),'b+');
        plot(x1(y1==-1,1),x1(y1==-1,2),'m+');

increment = 100;

domainx = xlim;
domain1 = domainx(1):(domainx(2)-domainx(1))/increment:domainx(2);
domainy = ylim;
domain2 = domainy(1):(domainy(2)-domainy(1))/increment:domainy(2);
d1 = repmat(domain1',[1 length(domain1)]);
d2 = repmat(domain2,[length(domain2) 1]);
d=[d1(:),d2(:)];
switch method
    case 'virtual examples'
        %yhat = rbf_kernel([d1(:),d2(:)],x1,sigma)*w;
        K=rbf_kernel(x1,d,sigma);
         yhat=predict(w,K)';
    case 'invariance constraint'
        dKplot=first_derivative_kernel_rotation(x_test,d, theta, sigma, k);
        Kplot=rbf_kernel(x_train,d,sigma);

        %yhat = [Kplot,dKplot']*w;
        yhat=predict(w,[Kplot;dKplot])';
    case 'invariance constraint and virtual examples'
        dKplot=first_derivative_kernel_rotation(x_test,d, theta, sigma, k);
        Kplot=rbf_kernel(x1,d,sigma);
        %yhat = [Kplot,dKplot']*w;
       
        yhat=predict(w,[Kplot;dKplot])';
end
        
z = reshape(yhat,size(d1));


contour(d1,d2,z+rand(size(z))/1000);
colorbar;




        plot(x_train(y_train==1,1),x_train(y_train==1,2),'bo');
        plot(x_train(y_train==-1,1),x_train(y_train==-1,2),'mo');
        plot(x1(y1==1,1),x1(y1==1,2),'b+');
        plot(x1(y1==-1,1),x1(y1==-1,2),'m+');

xlim(domainx);
ylim(domainy);
legend({'Class 1-Virtual Example','Class 2-Virtual Example'...
    ,method,'Class1','Class2'});
title(method);
hold off

end


