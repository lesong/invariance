function [ alpha ] = trainning_inv( a0,K,dK,d2K,y_train,lambda,tau,options)

 Kbig=tau*[K,dK';dK,d2K];
 K1=[K;dK];
 K2=lambda*[dK,d2K]'*[dK,d2K];

 len=length(a0);
 g=zeros(len,1);
 
 alpha=minFunc(@lossfun,a0,options,Kbig,K1,K2,y_train,g);   
 

end

