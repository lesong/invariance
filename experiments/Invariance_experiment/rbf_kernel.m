function [ K ] = rbf_kernel( x1,x2,sigma )
%x1, n1 by d matrix;
%x2, n2 by d matrix;
%K, n1 by n2 matrix;

    norm = sum(x1'.^2, 1)';
    norm1 = sum(x2'.^2, 1)';
    K = exp((2*x1*x2' - bsxfun(@plus, norm, norm1'))/(2*sigma.^2));


end

