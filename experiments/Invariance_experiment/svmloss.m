function [ L,g ] = svmloss(alpha1,K,y,g)
b=alpha1(1);
alpha=alpha1(2:end);
len=length(alpha1);
select=(alpha'*K+b)'.*y;
L=sum(max(0,1-select))+0.5*alpha'*K*alpha;

if nargout>1
  indicator=select<1;

  g(1,:)=sum(indicator.*(-y));
  g(2:end,:)=K*(indicator.*(-y))+ (K*alpha);
end


end

