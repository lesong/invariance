function [ ftest ] = predict( alpha,K_test)

a=alpha(2:end);
b=alpha(1);
ftest=a'*K_test+b;
end

