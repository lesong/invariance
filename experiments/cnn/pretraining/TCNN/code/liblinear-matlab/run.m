clear all
close all
%[y,xt] = libsvmread('../heart_scale');
m = 20000;
n = 1000;
 x1 = randn(m,n);
 x2 = randn(m,n) + 1000;
 x3 = randn(m,n) + 6000;
 xt = [x1;x2;x3];
 y = [ones(m,1); 2*ones(m,1); 3*ones(m,1)];
 xt = single(xt);
 y = single(y);
option_string = sprintf('-q -s 2 -B 1 -c %f',1 );
model=train(y, xt, option_string);
[l,a]=predict(y, xt, model);

