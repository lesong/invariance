train_stacked_tica_network(param) trains a tiled convolutional neural network using the TICA algorithm using the parameters defined in the "param" parameter structure. 

Saves both each layer during layerwise pretraining and a final network file at the end.

The fields of "param" are:
        num_layers: number of layers in the TICA network
    network_params: cell array of parameters for each layer of the TICA network
          datafile: path to the dataset file
          savepath: dir to save network and log files
 
network_params stores a cell array of parameter structures for each layer (see Tiled Convolutional Neural Networks NIPS2010 for more details):

                  tied_size: tile size
                window_size: size of local receptive field
                   num_maps: number of maps
                       step: convolution step size
               pooling_size: size of pooling neighbourhood
                   max_iter: maximum number of training iterations
                     l1_act: layer 1 activation function (@square_act for standard TICA)
                     l2_act: layer 2 activation function (@squareroot_act for standard TICA)
                random_seed: random seed for random initialization

datafile should be a .mat file with the following variables
                   	 X : input image in column example format where each column is a vectorized image in col-major form
                  input_ch : number of input channels of the image
                   datastr : name of dataset (for naming of savefile)

Examples of the param structure can be found in the provided file example_params.mat and an example of a valid datafile is found in example_data.mat


train_stacked_tica_network returns a stacked_network structure with the following fields:

        num_layers: number of layers
           layer_W: cell array of network weights
        layer_pool: cell array of neighbourhood pooling matrices
    network_params: cell array of network params


To use the trained network to generate features for linear SVM classification (liblinear), use:
classify_svm(network filename, training datafile, testing datafile);
where training and testing datafiles contain the variables:
		X: input vectors in col-major format
		Y: row vector of labels 
