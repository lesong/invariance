function [test_accuracy train_accuracy] = classify_svm(network_filename, train_datafile, test_datafile, C)
    global logfile;
    start;
    load(network_filename);

    if ~exist('C', 'var') || isempty(C)
        C = [0.0001, 0.001, 0.01, 0.1, 1, 10, 100];
    end

    load(train_datafile);
    % Y should be a col vector of labels; input is specified as a row vector for consistency with X
    Y = single(Y');
    % activations are returned in col-example format so each col is a set of activations for 1 example in the training set
    activations = stack_tica_forwardprop(X, stacked_network);
    clear X;

    % take the last layer and transpose so that it's now row-example 
    concat_activations = [];
    for a = 1 : stacked_network.num_layers
        concat_activations = [concat_activations single(activations{a, 2}')];
    end
    activations = concat_activations;
    clear concat_activations;

    % Cross validation of C with 5 fold cross-validation
    max_CV_accuracy = -1;
    for i = 1 : length(C)
    	option_string = sprintf('-q -s 2 -B 1 -c %f -v 5', C(i) );

    	CVAcc = train(Y, activations, option_string);
        fprintf('C %f   CVAcc %f\n', single(C(i)), single(CVAcc));
        printlog('C %f   CVAcc %f\n', single(C(i)), single(CVAcc));

        if max_CV_accuracy < CVAcc
            max_CV_accuracy = CVAcc;
            max_C = C(i);
        end

        train_accuracy(i) = CVAcc;
        if CVAcc > 99.9
            break;
        end
    end

    % Training the actual SVM model
    option_string = sprintf('-q -s 2 -B 1 -c %f', max_C);
    [model] = train(Y, activations, option_string);
    clear activations Y;

    load(test_datafile);
    Y = single(Y');
    activations = stack_tica_forwardprop(X, stacked_network);
    clear X;

    concat_activations = [];
    for a = 1 : stacked_network.num_layers
        concat_activations = [concat_activations single(activations{a, 2}')];
    end
    activations = concat_activations;
    clear concat_activations;

    fprintf('predicting\n');
    [predicted_labels, test_accuracy] = predict(Y, activations, model, '-b 0');
    fprintf('Best C %f   Test accuracy %f\n', max_C, single(test_accuracy));
    printlog('Best C %f   Test accuracy %f\n', max_C, single(test_accuracy));
end
