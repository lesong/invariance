function W = tica_train (savestr, param, stacked_network)

printLog('*** num maps = %d    window size = %d    step size = %d    tile size = %d\n', param.num_maps, param.window_size, param.step, param.tile_size);

% Initialize indices for rf windows, pooling and weight tying
[rf_index, pool_index, W, num_windows, h_dim, tied_units] = initialize_indices (param);

% Combines tied units into a single average or summed rf
collapsed_rf = collapse_rf (W, tied_units, param);

% Orthogonalize collapsed rfs looking at the same patch of the image
[collapsed_rf] = orthogonalize_collapsed_rf (param, collapsed_rf);

% Re-expand collapsed rfs back to original size
[W] = expand_rf (param, h_dim, tied_units, collapsed_rf);

printLog('Begin training');
iter = 0;
objs = [];
save(savestr, 'collapsed_rf','objs', 'iter');

alpha = 1;
new_alpha = 1.0/2^3;
linesearch_failed = 0;
while (iter < param.max_iter) && (linesearch_failed == 0)
    
    istart = tic;
    iter = iter + 1;
    alpha = new_alpha;    

    % Get tica objective and gradient for linesearch
    [obj, grad] = get_tica_obj_grad(stacked_network, full_size(W,rf_index), pool_index, param);
    grad_small = shrink(rf_index,grad);
    grad_small = collapse_rf(grad_small,tied_units,param);

    clear grad;

    objs = [objs obj];
    new_obj = -inf;
    inner_iter = 0;
    % Run linesearch until termination condition or max iteration
    while (new_obj < obj) && (inner_iter < 10)
        collapsed_rf_new = collapsed_rf + alpha*grad_small;
        collapsed_rf_new = orthogonalize_collapsed_rf (param, collapsed_rf_new);
        W_new = expand_rf (param, h_dim, tied_units, collapsed_rf_new);

        new_obj = get_tica_obj (stacked_network, full_size(W_new,rf_index), pool_index,param);
	printLog('iter = %d, alpha = %f, obj = %f, new_obj = %f\n', iter, ...
                alpha, obj, new_obj);
        new_alpha = alpha;
        alpha = alpha * 0.5;
        inner_iter = inner_iter + 1;
    end

    if (inner_iter >= 10)
      linesearch_failed = 1;
    else
      W = W_new;
      collapsed_rf = collapsed_rf_new;
    end
    
    % Periodically save progress
    if mod(iter,10) == 0
        save(savestr, 'collapsed_rf','objs', 'iter', '-append');
    end

    printLog('Elapsed time: %f\n',(toc(istart)));
end

save(savestr, 'collapsed_rf','objs', 'iter', '-append');
W = collapsed_rf;
save(savestr, 'W','-append');
printLog('Saved layer %d to %s\n', param.stacked_layer, savestr);



end
