function [collapsed_rf] = orthogonalize_collapsed_rf (param, collapsed_rf)
offset = 0;

for b = 1:param.tile_size^2        
	n = 0;
	for c = 0:param.num_maps-1
    		n = n + 1;
    		ortho_index(n) = b + c*(param.tile_size^2);
	end        
	collapsed_rf(ortho_index,:) = orthogonalizerows(collapsed_rf(ortho_index,:));        
end    

end
