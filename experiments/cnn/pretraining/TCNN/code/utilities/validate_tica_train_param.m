function [param] = validate_tica_train_param(param)

    if ~isfield(param, 'pooling_size')
        param.pooling_size = 1;
    end

    if ~isfield(param,'num_maps')
        param.num_maps = 1;
    end

    if ~isfield(param,'window_size')
        param.window_size = 8;
    end

    if ~isfield(param,'step')
        param.step = 1;
    end

    if ~isfield(param,'tile_size')
        param.tied_size = 1;
    end

    if ~isfield(param, 'l1_act')
	param.l1_act = @square_act;
    end

    if ~isfield(param, 'l2_act')
	param.l2_act = @squareroot_act;
    end

    if ~isfield(param,'random_seed')
        param.random_seed = 0;
    end

    if ~isfield(param,'max_iter')
        param.max_iter = 3;
    end

    if ~isfield(param,'untie_init')
	param.untie_init = 0;
    end
end
