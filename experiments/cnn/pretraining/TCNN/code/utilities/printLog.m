function [] = printLog(varargin)
        global logfile
        if isempty(logfile)
		sprintf('\n')
		sprintf(varargin{:})
        else
                [fid,msg] = fopen(logfile,'a');
		fprintf(fid,'\n');
		fprintf(fid,varargin{:});
		fclose(fid);
	end	
end

