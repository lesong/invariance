function [descriptor] = generate_layer_descriptor (param)
% Generates a descriptor string given hyperparameters
descriptor = sprintf('_m_%d_ws_%d_ts_%d_st_%d_fun_%s_%s_rs_%d_MI_%d', param.num_maps, param.window_size, param.tile_size, param.step, func2str(param.l1_act), func2str(param.l2_act),param.random_seed,param.max_iter);

end
