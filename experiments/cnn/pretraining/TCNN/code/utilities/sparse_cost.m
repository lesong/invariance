function [a, grad] = sparse_cost(X)
% Cost function for TICA

a = -sum(X(:));
if nargout > 1
      grad = single(-ones(size(X)));
end
