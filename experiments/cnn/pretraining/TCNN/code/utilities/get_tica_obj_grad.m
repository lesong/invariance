function [obj, grad_layer1, grad_layer2] = get_tica_obj_grad(stacked_network, W, pool_index, param)

global X;

T = param.num_examples;
batchsize = 6000; % number of examples in one minibatch

indx = round(linspace(0,T,ceil(T/batchsize)+1));
obj = 0;

if nargout > 1
	grad_layer1 = zeros (size(W,1),param.image_size^2*param.input_ch);
end

if nargout > 2
    grad_layer2 = zeros(size(pool_index,1));
end

if nargout == 1
    for i=1:length(indx)-1
        [dummy, layer2_activations] = two_layer_forwardprop(X(:,indx(i)+1:indx(i+1)), W, pool_index, param.l1_act, param.l2_act);
        sparse_obj = sparse_cost(layer2_activations);
        obj = obj + sparse_obj;
    end   
end

% Compute gradients only if the function calling get_tica_obj requires it
if nargout > 1
    for i=1:length(indx)-1
        [layer1_activations, layer2_activations, temp_grad1, temp_grad2] = two_layer_forwardprop_grad(X(:,indx(i)+1:indx(i+1)), W, pool_index, param.l1_act, param.l2_act);
        [sparse_obj,sparse_grad] = sparse_cost(layer2_activations);
        obj = obj + double(sparse_obj);
        if nargout > 2        
            grad_layer2 = grad_layer2 + (sparse_grad.*temp_grad2)*layer1_activations';
        end
        grad_layer1 = grad_layer1 + ( (full(pool_index')*(sparse_grad.*temp_grad2) ).*temp_grad1)* X(:,indx(i)+1:indx(i+1))';
    end
end
        
obj = obj / T;

if nargout > 1
	grad_layer1 = grad_layer1 / T;
end

if nargout > 2 
    grad_layer2 = grad_layer2 / T; 
end

end
