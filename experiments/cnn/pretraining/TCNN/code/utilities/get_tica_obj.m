function [obj] = get_tica_obj(stacked_network, W, pool_index, param)

global X;

T = param.num_examples;
batchsize = 6000; % number of examples in one minibatch

indx = round(linspace(0,T,ceil(T/batchsize)+1));
obj = 0;

for i=1:length(indx)-1
	[dummy, layer2_activations] = two_layer_forwardprop(X(:,indx(i)+1:indx(i+1)), W, pool_index, param.l1_act, param.l2_act);
	sparse_obj = sparse_cost(layer2_activations);
	obj = obj + sparse_obj;
end   

obj = obj / T;

end
