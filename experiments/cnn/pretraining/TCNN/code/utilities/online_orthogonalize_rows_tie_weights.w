function W = online_orthogonalize_rows_tie_weights(W, innermap_size)
m = size(W,1) / innermap_size / innermap_size; 
assert(m == round(m),'m is not rounded');
d = innermap_size*innermap_size;
for offset=1:d
    temp_indices = zeros(m,1);
    for i=1:m
        temp_indices(i) = offset + (i-1)*innermap_size * innermap_size; 
    end
%     temp_indices
     W(temp_indices,:) = orthogonalizerows(W(temp_indices,:));    
    %online_orthogonalize_rows(W(temp_indices,:), indices(temp_indices,:),
    %num_iter, num_channels, see_all_input_channels);
end

% fprintf('max = %f, min = %f\n', max(max(abs(W))),min(min(abs(W))));