function train_stacked_tica_network (param)
% Trains a multilayer local topographic ICA network on square images
% 
% Structure of param struct:
%        num_layers: number of layers in the TICA network
%    network_params: cell array of parameters for each layer of the TICA network
%          datafile: path to the dataset file
%	   savepath: dir to save network and log files
% 
% Parameters for each layer (see Tiled Convolutional Neural Networks NIPS2010 for more details):
%
%	           tied_size: tile size
%	         window_size: size of local receptive field
%	            num_maps: number of maps
%	                step: convolution step size
%	        pooling_size: size of pooling neighbourhood
%	            max_iter: maximum number of training iterations
%	              l1_act: layer 1 activation function (@square_act for standard TICA)
%	              l2_act: layer 2 activation function (@squareroot_act for standard TICA)
%	         random_seed: random seed for random initialization
%
%
% datafile is be a .mat file with the following variables
%         	    X : input image in column example format where each column is a vectoriszed image in col-major form
%            input_ch : number of input channels of the image
%             datastr : name of dataset (for naming of savefile)

start;
global X;
global logfile;

if isfield(param,'savepath')
	path = param.savepath;
else
	path = './save_files/';
end
savestr = [];

for a = 1:param.num_layers
    layer_param = param.network_params{a};
    layer_param = validate_tica_train_param(layer_param);
    descript = generate_layer_descriptor(layer_param);
    savestr = [savestr, '_l' num2str(a) descript];
end


% Load dataset
fprintf('loading data...');
load(param.datafile);

% Create save directory
savepath= [path datastr '/'];
mkdir(savepath);
mkdir([savepath 'log/']);

logfile = [savepath 'log/' datastr savestr '_network.log']
network_savestr = [savepath datastr savestr '_network.mat'];

printLog('Saving to network file: %s\n',network_savestr);
sprintf('Writing to logfile: %s\n',logfile);
printLog('Loaded dataset');

image_size = sqrt(size(X,1)/input_ch);
num_examples = size(X,2);

% Initialize stacked network
stacked_network = struct;
stacked_network.layer_W = cell(param.num_layers,1);
stacked_network.layer_params = cell(param.num_layers,1);
stacked_network.num_layers = 0;

% Greedily train network layer by layer
for a = 1:param.num_layers
    
    % Initialize layer parameters
    layer_param = param.network_params{a};
    layer_param.input_ch = input_ch;
    layer_param.image_size = image_size;
    layer_param.stacked_layer = a;
    layer_param.num_examples = num_examples;
    
    % Validate layer params and generate descriptor for filename
    layer_param = validate_tica_train_param(layer_param);
    descript = generate_layer_descriptor(layer_param);
    
    % Train a layer
    printLog('### Training layer %d of stacked network\n',a);
    savestr = [savepath datastr descript '_l' num2str(a) '.mat'];
    tstart = tic;
    W = tica_train(savestr, layer_param, stacked_network);
    printLog('Elapsed time: %f\n',(toc(tstart)));
    
    % Update parameters for next layer
    [rf_index, pool_index, dummy, dummy, h_dim, tied_units] = initialize_indices (layer_param);
    image_size = h_dim;
    input_ch = size(pool_index,1)/(image_size^2);

    % Forwardprop X through current layer to generate input for next layer
    W_temp = expand_rf (layer_param, h_dim, tied_units, W);
    W_temp = full_size (W_temp,rf_index);
    [dummy, X] = two_layer_forwardprop(X, W_temp, pool_index, @square_act, @squareroot_act); 
    
    % Append current layer to the stacked network
    stacked_network.layer_pool{a} = pool_index;
    stacked_network.layer_W{a} = W;
    stacked_network.network_params{a} = layer_param;
    stacked_network.num_layers = stacked_network.num_layers + 1;
end

% Save stacked network
stacked_network.type = 'stacked';

save(network_savestr, 'stacked_network');
printLog('Saved stacked network to %s\n', network_savestr);
clear X;

end
