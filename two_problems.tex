\documentclass{article}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}

\usepackage{graphicx} % more modern

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{enumitem}
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% \usepackage{fullpage}
% \usepackage{authblk}

% other packages
\usepackage{Definitions}
\usepackage{color}

\newcommand{\Le}[1]{{\color{red} [Le: #1]}}



\setlength\parindent{0pt}
\setlength{\parskip}{1em}

\title{Theoretical Foundation of Deep Learning}

\author{Le Song (CSE) and Santosh Vempala (CS)}

\date{}

\begin{document}
\maketitle


% \begin{abstract}
% \end{abstract}

% The problem. How is it being down today. 
{\bf Background}

Modern machine learning problems, such as image classification, speech recognition, text categorization, and drug prediction, are becoming increasingly complex. For instance, the ImageNet classification problem involves thousands of tasks, tens of thousands of input dimensions, and millions of data points. Solving such problems with machine learning helps us better understand and organize information in the world, and 
promises significant advances for our society. At the same time, the unprecedented scale and complexity of these problems beg for sophisticated feature representation of the data, and efficient algorithms which can adapt to the complexity of the data. 

{\bf The Problem}

In recent years, deep neural networks have emerged as the state-of-the-art techniques to address some of the largest scale and the most complex machine learning problems, due largely to their empirically successes in image classification~\cite{JarKavRanLeC09,KriSutHin12}, speech recognition~\cite{HinDenYuDahetal12} and drug prediction~\footnote{\url{http://blog.kaggle.com/2012/11/01/deep-learning-how-i-did-it-merck-1st-place-interview/}}. However, in stark contrast to their empirical successes, theoretical analysis and understanding of deep neural networks remains murky and sparse. So far, deep neural networks have been largely treated as blackboxes, and choosing the network architecture and tuning the model parameters have been typically carried out by trial-and-error without principled theoretical guidelines. 

What are the differences between modern deep neural networks and those neural networks in 80's and 90's? 
Why do deep neural networks perform so well? How to choose the architecture for these new networks? How to efficiently and provable learn the parameters in these highly non-convex models? Is there any other alternative and principled machine learning algorithms which can provide comparable performance to deep neural networks? 

{\bf The Goal} 

Motivated by these questions and our own experiments, the goal of the project is to provide theoretical foundation to several key aspects of deep learning, and provide empirical evidence to support these theoretical analysis. Furthermore, we aim to design principled new models, potentially simpler and more interpretable than deep neural networks, and the corresponding efficient learning algorithms which can achieve comparable performances. 

% Draw the pictorial architecture. 
\begin{figure}[h!]
%   \vspace{-2mm}
\fbox{
  \begin{minipage}{15cm}
  \includegraphics[width=0.98\textwidth]{deeparchitecture-crop}
  \vspace{-4mm}
  \caption{\small Architecture of an 8 layer convolution neural network model which produces the state-of-the-art results in ImageNet classification problem with 1000 classes and 1.3 million data points. A 224 by 224 crop of an image (with 3 color planes) is presented as the input. This is convolved with 96 different 1st layer filters (red), each of size 7 by 7, using a stride of 2 in both $x$ and $y$ coordinates. The resulting feature maps are then: (i) passed through a rectified linear function, $\max\cbr{0, x}$ (not shown), (ii) pooled (max within 3x3 regions, using stride 2) and (iii) optionally contrast normalized across feature maps to give 96 different 55 by 55 element feature maps. Similar operations are repeated in layers 2,3,4,5. All filters and feature maps are square in shape. The last two layers are fully connected, taking features from the top convolutional layer as input in vector form ($6\times 6 \times 256 = 9216$ dimensions). The final layer is a $C$-way softmax function, 
$C$ being the number of classes. The fully connected layers contain 94\% of the parameters in the model needed to be learned.}
%   \vspace{-2mm}
  \label{fig:deeparchitecture}
  \end{minipage}  
}
\end{figure}

% Point out the key components. 
{\bf Proposed Work} 

To put our proposed work in context, we will first delve briefly into the modern architecture of 
deep neural networks for image classification shown in Figure~\ref{fig:deeparchitecture}. A state-of-the-art deep convolutional network consists of two types of layers, each with very different properties. Convolution layers, which contain a small fraction of the network parameters, represent most of the computational effort. In contrast, fully connected layers contain the vast majority of the parameters but are comparatively cheap to evaluate. Furthermore, these two types of layers are designed with very different purposes: the convolution layers are supposed to extract invariance image features, while the fully connected layers are supposed to perform nonlinear multi-class classifications. Given this gigantic model, the learning of the model parameters are carried out via stochastic gradient descent nowadays where a small batch of data points are used to calculate the gradients of a multi-class logistic loss function, and update the model parameters. Experimentally, it seems that the batch size has to be 
small enough in order for the algorithm to converge fast to a set of good model parameters.  

We propose to investigate the following questions: 
\begin{enumerate}
  \item Our experiments showed that the fully connected layers can be replaced by kernel classifiers and the network can still achieve the same performance~\cite{DaiXieHeLiaEtAl14}. These experiments suggest that the key to the performance of deep neural network is the convolution and pooling layers. We propose to replace the learned convolution filters by random filters and study the convolution and pooling from a random projection point of view~\cite{Vempala04,KanVem09}. The hypothesis is that the structure of interleaving convolution and pooling layers is more important for extracting translation invariant features than the actual filter values. In particular, we will investigate the case that a discriminative signal with a compact support is translated, and analyze whether the margin of the classifier will be boosted after random convolution and pooling operations. This is an interesting case where random projections are applied in a structured (hierarchical) way to capture certain aspect of a signal but ignore certain other aspect. 
  
  \item The optimization problem for learning the parameters in deep neural networks is a non-convex problem. One effective way of learning the parameters is to use stochastic gradient descent where the gradient is computed using a small batch of examples. The hypothesis is that the batch size has to be ``just right'' in order for the optimization to convergence fast. Essentially, the appropriate batch size provide the right amount of variance in the gradient to help the algorithm escape the basin of attractors of local optima. We will analyze the size of the basin of attractors of realistic deep neural networks and establish the relation between the batch size and ability of the algorithm escaping the local optima in such models. 

  \item The architecture of deep neural network and the number of parameters are fixed before training, and the entire model needs to be retrained when a previous unseen classification task are presented. We propose to investigate multiclass classifiers with growing capability, where the number of parameters of model can grow as new classification tasks are presented but these tasks share some low dimensional common structures. 
  In particular, we will investigate the case where the classification task are presented one by one, and the initial classifier is learned from the random convolution and pooling features, and later classifiers can either build upon the output of previous classifiers or the random convolution and pooling features. 
  Furthermore, we will also investigate the case where the number of random convolution filters are increased as most task are presented, which will potentially allow the algorithm to better adapt to the increasing complexity of the data. 
\end{enumerate}

The proposed methods will be experimented on both large scale image classification problem and materials structure design problem
\begin{enumerate}
  \item ImageNet 2012 dataset, where the task is to predict the main object in an image. This dataset contains 1.3 million color images from 1000 classes\footnote{\url{http://www.image-net.org/}}. 
  \item MolecularSpace dataset, where the task is to predict the power conversion efficiency (PCE) of the molecule. This dataset of 2.3 million molecular motifs is obtained from the Clean Energy Project Database\footnote{\url{http://www.molecularspace.org/}}.
\end{enumerate}


% More specifically, let $K \in \RR^{n\times n}$ be a convolution filter (for simplicity of notation, we assume the filter size $n$ is an odd number), and the input image be $X_0 \in \RR^{m\times m}$, the convolution operation is carried out like this
% \begin{align}
%   \Xtil_1 = f(K \star X_0)\quad\text{where}\quad (K \star X_0)(i,j)~~= \sum_{i',j'=-\lfloor \smallfrac{n}{2} \rfloor}^{\lfloor \smallfrac{n}{2} \rfloor} K\rbr{\lfloor \smallfrac{n}{2} \rfloor+i',\lfloor \smallfrac{n}{2} \rfloor+j'}\, X_0\rbr{i+i',j+j'}
% \end{align}
% and $f(\cdot)$ is a nonlinear activation function applied elementwisely to input matrix. A commonly used nonlinear activation function is the rectified linear unit defined as $f(x) = \max\cbr{0, x}$. After this convolution and nonlinear activation function, the pooling operation is applied to a window of size $\ell$ 
% \begin{align}
%   X_1(i,j) = \max \cbr{ \Xtil_1(i+i',j+j') }_{i',j'=-\lfloor \smallfrac{\ell}{2} \rfloor}^{\lfloor \smallfrac{\ell}{2} \rfloor}
% \end{align}

% New technical idea. Why can we succeed now? 

% What is the impact if successful? 

% How will the research be organized? 

% How will the intermediate results be measured? 

% What will it cost? 

{\bf PIs qualification and complementary expertise}

Overall, this proposal combines in a unique way the complementary expertise of both PIs and their students, and uses tools from theoretical and empirical machine learning.

PI Vempala brings unique expertise in theoretical machine learning. His past work has developed new foundations and provably correct algorithms for several machine learning paradigms.

PI Song brings unique expertise in practical and large-scale machine learning algorithms and their applications in diverse real-world problems. He has also done substantial work on nonparametric kernel methods which will also be directly relevant to the project. Nonparametric methods allow us to adaptively learn the classification model from the data without restricting to pre-specified parametric assumptions. 

{\bf College-wide impact} 

Vempala and Song have already started collaborative research. Furthermore, Song has also served in the thesis committee of several CS students doing their PhD in theoretical machine learning. This fellowship will encourage further collaborative efforts and strengthen Georgia Tech's expertise in the theory and practice of machine learning and big data analysis. 

Vempala and Song also plan to advise two graduate students for this project. The fellowship will be shared between Bo Xie and Hanjun Dai. This collaboration gives a unique opportunity students to connect theory and practice, and exchange ideas and experiences, and more generally provides students from the College of Computing a more well-balanced PhD training.

To further encourage cross-departmental student collaboration, we will also organize a reading group on topics relevant to this proposal; we will also invite external worldwide experts as speakers for the Machine Learning seminar and for the ARC colloquium.

Big Data and Machine Learning are important emerging area, which promise great impact in our everyday life and national economy. Georgia Tech is in a unique position to take the lead in this field: we have many worldwide Machine Learning experts in various specific areas in different departments across College of Computing, Math and ISYE. While College of Computing faculty is organizing the premier machine learning conference, International Conference on Machine Learning, we has faced a challenge of having a cohesive and unified research efforts in Machine Learning. This project represents an extremely important step for fostering interactions which are crucial for establishing Georgia Tech as a top worldwide Machine Learning center.

{\bf Potential impact on research, industry and applications}

Our work brings new directions to the research and practice of complex and non-convex machine learning models, and has the potential to greatly broaden the relevance of principled machine learning algorithms to real world applications, such as image and video analysis, and 3D molecular and materials structure analysis. Our research has the potential to provide  crucial missing bridge between theory and practice of complex learning problems. The algorithmic aspect and applications to real world data will also allow us to impact a broad range of industrial and governmental sectors, such as image and video indexing, and drug and materials design.   

\bibliographystyle{unsrt}
% \bibliography{../../../bibfile2013/bibfile}
\begin{thebibliography}{1}

\bibitem{JarKavRanLeC09}
K.~Jarrett, K.~Kavukcuoglu, M.~Ranzato, and Y.~LeCun.
\newblock What is the best multi-stage architecture for object recognition?
\newblock {\em International Conference on Computer Vision}, 2146--2153, 2009.

\bibitem{KriSutHin12}
A.~Krizhevsky, I.~Sutskever, and G.~Hinton.
\newblock Imagenet classification with deep convolutional neural networks.
\newblock In {\em NIPS}, 2012.

\bibitem{HinDenYuDahetal12}
Geoffrey~E. Hinton, Li~Deng, Dong Yu, George~E. Dahl, Abdel{-}rahman Mohamed,
  Navdeep Jaitly, Andrew Senior, Vincent Vanhoucke, Patrick Nguyen, Tara~N.
  Sainath, and Brian Kingsbury.
\newblock Deep neural networks for acoustic modeling in speech recognition: The
  shared views of four research groups.
\newblock {\em IEEE Signal Process. Mag.}, 29(6):82--97, 2012.

\bibitem{DaiXieHeLiaEtAl14}
B.~Dai, B.~Xie, N.~He, Y.~Liang, A.~Raj, M.~Balcan, and L.~Song.
\newblock Scalable kernel methods via doubly stochastic gradients.
\newblock In {\em Neural Information Processing Systems}, 2014.

\bibitem{Vempala04} 
S.~Vempala.
\newblock The random projection method. 
\newblock {\em DIMACS series in discrete mathematics and theoretical computer science, AMS}, 65, 2004. 

\bibitem{KanVem09} 
R.~Kanna, S.~Vempala. 
\newblock Spectral Algorithms.
\newblock {\em Found. Trends Theor. Comput. Sci.}, 4(3-4):157--288, 2009.

\end{thebibliography}

\end{document}
