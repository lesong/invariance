function [Xtr Ytr Xte Yte]=loadColorCIFAR10(datapath)
Xtr=zeros(32,32,3,50000);
Ytr=zeros(1,50000);
for ii=1:5
   load(sprintf('%s/cifar10/cifar-10-batches-mat/data_batch_%d.mat',datapath, ii));
   Ytr((ii-1)*10000+1:ii*10000)=double(labels);
   Xtr(:,:,:,(ii-1)*10000+1:ii*10000)=double(reshape(data(:,:)',[32 32 3 10000]))/255;
end

load(sprintf('%s/cifar10/cifar-10-batches-mat/test_batch.mat', datapath));
whos
Ytr=Ytr';
Yte=double(labels);
Xte=double(reshape(data',[32 32 3 10000]))/255;

Xte=reshape(Xte,[32 32*3 10000]);
Xtr=reshape(Xtr,[32 32*3 50000]);
