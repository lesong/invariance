datapath='/nv/hcoc1/hdai8/data/dataset';
software_path = '/nv/hcoc1/hdai8/data/Software';
addpath([software_path, '/liblinear-1.5-dense/matlab/']); % you need to compile the mex files from this folder
addpath([software_path, '/spams-matlab/build']);
addpath([software_path, '/lbfgsb3.0_mex1.2']); % you also need to compile the mex files from this folder

% We provide here some commands corresponding to experiments conducted 
% in the paper. There are minor differences in the results due to
%    - small change in the contrast normalization step that gives slightly
%      better results.
%    - random seeds that are reinitialized to zero between each experiments
%      here. That was not the case for the experiments conducted in the paper.


% experiment 1 for mnist - CKN-PM2
% 2 layers, working on grayscale patches without centering
%   1) 5 x 5 patches, 50 filters, subsampling factor=2 
%   2) 2 x 2 patches, 200 filters, subsampling factor=2 
% the final image representation is of size 200 x 6 x 6 = 7200
%test_ckn([5 2 0],[2 2 0],[50 200 0],2,'mnist', datapath);
% the classification error rate obtained here is 0.57% (0.53% in the paper)


% experiment 2 for mnist - CKN-PM1
% 2 layers, working on gradient maps
%   1) 1 x 1 patches, 12 filters, subsampling factor=2 
%   2) 3 x 3 patches, 50 filters, subsampling factor=2 
%test_ckn([1 3 0],[2 2 0],[12 50 0],3,'mnist', datapath);
% the final image representation is of size 50 x 6 x 6 = 1800
% the classification error rate obtained here is 0.53% (0.63% in the paper)


% experiment 3 for mnist - CKN-GM2
% 2 layers, working on gradient maps
%   1) 1 x 1 patches, 12 filters, subsampling factor=2 
%   2) 3 x 3 patches, 400 filters, subsampling factor=4 
% the final image representation is of size 400 x 3 x 3 = 3600
%test_ckn([1 3 0],[2 4 0],[12 400 0],3,'mnist', datapath);
% the classification error rate obtained here is 0.41% (0.39% in the paper)

% todo 
% test_ckn([1 3 0],[2 3 0],[12 50 0],3,'mnist');


% experiment 1 for cifar-10 - CKN-GM
% 2 layers, working on gradient maps
%   1) 1 x 1 patches, 12 filters, subsampling factor=2 
%   2) 2 x 2 patches, 800 filters, subsampling factor=4 
% the final image representation is of size 800 x 4 x 4 = 12800
test_ckn([1 2 0],[2 4 0],[12 800 0],3,'cifar-10', datapath);
% the classification accuracy obtained here is 75.49% (74.84% in the paper)


% experiment 2 for cifar-10 - CKN-PM
% 2 layers, working on color patches after centering 
%   1) 2 x 2 patches, 100 filters, subsampling factor=2 
%   2) 2 x 2 patches, 800 filters, subsampling factor=5 
% the final image representation is of size 800 x 3 x 3 = 7200
test_ckn([2 2 0],[2 4 0],[100 800 0],1,'cifar-10', datapath);
% the classification accuracy obtained here is 79.29% (78.30% in the paper)

% todo
% test_ckn([2 2 0],[2 4 0],[100 800 0],0,'cifar-10');

% experiment 3 for cifar-10 - variant of CKN-CO
% learn three individual models
test_ckn([2 2 0],[2 5 0],[100 800 0],1,'cifar-10', datapath); % this is a two-layer model on patches
test_ckn([3 0 0],[10 0 0],[800 0 0],0,'cifar-10', datapath); % this is a one-layer model
test_ckn([1 2 0],[2 5 0],[12 800 0],3,'cifar-10', datapath); % this is on patches
test_ckn_combine; % combine the three models.
% the classification accuracy obtained here is 82.23% (82.18% in the paper)

