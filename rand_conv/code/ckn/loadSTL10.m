function X = loadSTL10()
load('data/stl10_matlab/unlabeled.mat');
X=X';
X=reshape(X,[96 96*3 100000]);
