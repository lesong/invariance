function [outmap] = subsampling(inmap,n);
filt=fspecial('gaussian',[1 2*n+1],n/sqrt(2));
outmap=inmap;
for ii=1:size(outmap,3)
   outmap(:,:,ii)=conv2(filt,filt,outmap(:,:,ii),'same');
end

offset=ceil(n/2);
outmap=outmap(offset:n:end,offset:n:end,:);

