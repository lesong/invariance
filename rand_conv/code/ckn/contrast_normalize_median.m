function X = contrast_normalize_median(X);
nrm=sqrt(sum(X.^2));
med=median(nrm);
X=bsxfun(@rdivide,X,max(nrm,max(med,0.00001)));
