function out = get_zeromap(in,type_zerolayer)
if ~isfloat(in)
   in=double(in)/255;
end
sx=size(in,1);
sy=size(in,2);
if 3*sx == sy %assume this means rgb 
   if type_zerolayer==3
      out=in(:,sx+1:2*sx); % get the green channel
   else
      out=reshape(in,[sx sx 3]);
   end
else
   out=in;
end

