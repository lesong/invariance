function Y = centering(X,nchannels)
sizechannel=size(X,1)/nchannels;
Y=X;
for ii=1:nchannels
   XX=X((ii-1)*sizechannel+1:ii*sizechannel,:);
   Y((ii-1)*sizechannel+1:ii*sizechannel,:)=bsxfun(@minus,XX,mean(XX));;
end
