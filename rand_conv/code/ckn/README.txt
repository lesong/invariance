- go to the folder data and follow instructions in README.txt
- download and install the SPAMS toolbox http://spams-devel.gforge.inria.fr/downloads.html
  the dependency to this toolbox will be removed in a future release
- install the l-bfgs-b solver: go to the folder Lbfgsb.3.0 and compile the mex files
- install the liblinear solver: go to the folder liblinear-dense and compile the mex files


the script run_experiments.m contains instruction to run experiments from the NIPS paper.
The folder logs/ contains some models that have been already learned.
