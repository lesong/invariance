function model = training(Input,param,dataset)
npatches=param.npatches;
smaps=param.smaps;
quantiles=param.quantiles;
subs_factors=param.subs_factors;
type_zerolayer=param.type_zerolayer;
model.nlayers=nnz(npatches);
nlayers=model.nlayers;

model.layer=cell(nlayers,1);
model.type_zerolayer=param.type_zerolayer;
for ii=1:nlayers
   fprintf('Training of layer %d\n',ii);
   model_layer.numlayer=ii;
   model_layer.centering=type_zerolayer <=1 && ii==1;
   model_layer.median_contrast_normalization=type_zerolayer ==0 && ii==1;
   model_layer.npatch=npatches(ii);
   model_layer.subsampling=subs_factors(ii);
   model_layer.smap=smaps(ii);
   model_layer.type_zerolayer=type_zerolayer;

   if ii==1 && type_zerolayer ==3 
      % if this is the gradient map, there is no learning
      model.layer{ii}=model_layer;
      continue;
   end

   fprintf('Create the training set\n');
   for jj=1:size(Input,3)
      fprintf('%d\r',jj);
      psi=get_zeromap(Input(:,:,jj),type_zerolayer);
      for kk=1:ii-1
         psi=encode_layer(psi,model.layer{kk});
      end
      nchannels=size(psi,3);
      psi=mexExtractPatches(psi,npatches(ii),1);

      % memory allocation for X
      if jj==1
         per_image=min(floor(2*param.ntrain/size(Input,3)),size(psi,2));
         X=zeros(size(psi,1),per_image*size(Input,3));
      end

      % discard patches with no variance
      psiC=bsxfun(@minus,psi,mean(psi));
      nrm=sqrt(sum(psiC.^2)); 
      ind=find(nrm);
      if per_image <= length(ind)
         psi=psi(:,ind);
      end

      % select random patches from the map
      per=randperm(size(psi,2));
      psi=psi(:,per(1:per_image));
      X(:,(jj-1)*per_image+1:jj*per_image)=psi;
   end

   fprintf('Contrast normalization\n');
   if model_layer.centering
      X=centering(X,nchannels);
   end
   if model_layer.median_contrast_normalization
      X=contrast_normalize_median(X);
   else
      X=contrast_normalize(X);
   end

   n=floor(size(X,2)/2);
   per=randperm(size(X,2));
   Y=X(:,per(n+1:2*n));
   X=X(:,per(1:n));
   fprintf('Learn sigma\n');
   tmp=sqrt(sum((Y-X).^2));
   model_layer.sigma=quantile(tmp,quantiles(ii));
   
   fprintf('Train the filters\n');
   [Z w] = learn_kernel_pairs(X,Y,model_layer.sigma,model_layer.smap);
   model_layer.Z=Z;
   model_layer.w=w;
   model.layer{ii}=model_layer;
end

% get the image description size
psi=get_zeromap(Input(:,:,1),type_zerolayer);
for kk=1:nlayers
   psi=encode_layer(psi,model.layer{kk});
end
psi=psi(:);
model.ndesc=length(psi);
