function [Xtr Ytr Xte Yte]=loadSTL10(fold)
load('data/stl10_matlab/test.mat');
Xte=X';
Yte=y-1;
Xte=reshape(Xte,[96 96*3 8000]);
load('data/stl10_matlab/train.mat');
Xtr=X(fold_indices{fold},:)';
Xtr=reshape(Xtr,[96 96*3 1000]);
Ytr=y(fold_indices{fold});
Ytr=Ytr-1;

