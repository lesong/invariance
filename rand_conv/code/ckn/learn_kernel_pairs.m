function [Z w Psi] = learn_kernel_pairs(X,Y,sigma,d)
% matlab initialization
format compact;

% evaluate the kernel
K=gaussian_kernel(X,Y,sigma);
sumK=mean(K.^2);
K=K/sqrt(sumK);

% initialize the Z's and w
[Z tmp] = mykmeans(X,d);
w = (1/d)*ones(d,1);
PsiX = updatePsi(X,Z,sigma); 
PsiY = updatePsi(Y,Z,sigma);  
w = updateW(K,PsiX,PsiY,w,20);

%%%%
obj = objW(w,K,PsiX,PsiY);
fprintf('Initial objective function after updating weights: %d \n',obj);
%
[Z w] = updateZW(X,Y,K,sigma,Z,w);
PsiX = updatePsi(X,Z,sigma); 
PsiY = updatePsi(Y,Z,sigma);  
obj = objW(w,K,PsiX,PsiY);
fprintf('Objective function: %d \n',obj);
w = updateW(K,PsiX,PsiY,w,1000);

PsiX = updatePsi(X,Z,sigma); 
PsiY = updatePsi(Y,Z,sigma);  
obj = objW(w,K,PsiX,PsiY);
fprintf('Objective function: %d \n',obj);

end

function [K] = gaussian_kernel(X,Y,sigma)
K=sum((X-Y).^2);
K = exp(-K/(2*sigma^2));
end


function [Z w] = updateZW(X,Y,K,sigma,Z,w)
ZW=[Z;w'];
L=-inf*ones(size(ZW));
L(end,:)=0;
U=inf*ones(size(ZW));
L=L(:);
U=U(:);
ZW=ZW(:);
opts.x0=ZW;
opts.maxIts=4000;
opts.maxTotalIts=10000;
opts.pgtol=1e-8;
opts.m=1000;
opts.printEvery=10;
fun=@(x) objZW(x,size(Z,2),X,Y,K,sigma);
tic
ZW=lbfgsb(fun,L,U,opts);
toc
ZW=reshape(ZW,[size(Z,1)+1 size(Z,2)]);
Z=ZW(1:end-1,:);
w=ZW(end,:)';
end

function [w] = updateW(K,psiX,psiY,w,it)
n=length(w);
opts.x0=w;
opts.maxIts=it;
opts.m=20;
opts.printEvery=10;
opts.pgtol=1e-8;
fun=@(x) objW(x,K,psiX,psiY);
w=lbfgsb(fun,zeros(n,1),inf*ones(n,1),opts);
end

function [psi] = updatePsi(X,Z,sigma)
n=size(X,2);
d=size(Z,2);
nrmsX=sum(X.^2);
nrmsZ=sum(Z.^2);
psi = nrmsZ' * ones(1,n) + ones(d,1)*nrmsX - 2*Z'*X;
psi = exp(-psi/(sigma^2));
end

function [obj] = evalObj(K,psiX,psiY,w)
psiw = diag(w) * psiX;
R = K - sum(psiw .* psiY);
obj =mean(R.^2);
end

function [obj grad] = objZW(ZW,d,X,Y,K,sigma)
p=length(ZW)/d;
n=length(K(:));
ZW=reshape(ZW,[p d]);
w=ZW(end,:);
Z=ZW(1:end-1,:);
psiX = updatePsi(X,Z,sigma);
psiY = updatePsi(Y,Z,sigma);
psiIJ= psiX .* psiY;
R=K-w*psiIJ;
obj =mean(R.^2);
if nargout==2
   KK=bsxfun(@times,psiIJ,R);
   sumKK=sum(KK,2);
   gradw=-(2/n)*sumKK;
   tmp=X + Y;
   gradZ = bsxfun(@times,- tmp*KK' + 2*Z*diag(sumKK),(4/(n*sigma*sigma))*w);
   gradZW=[gradZ; gradw'];
   grad=gradZW(:);
end
end

function [obj grad] = objW(w,K,psiX,psiY)
psiIJ = psiX .* psiY;
R=K-w'*psiIJ;
obj =mean(R.^2);
n=length(K);
if nargout==2
   grad=-(2/n)*psiIJ*(R');
end
end
