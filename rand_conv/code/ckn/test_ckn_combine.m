clear all;
datapath='/home/hanjun/data/dataset';
software_path = '/home/hanjun/Software';
addpath([software_path, '/liblinear-1.5-dense/matlab/']); % you need to compile the mex files from this folder
addpath([software_path, '/spams-matlab/build']);
addpath([software_path, '/lbfgsb3.0_mex1.2']); % you also need to compile the mex files from this folder

[Xtr Ytr Xte Yte]=loadCIFAR10(datapath);

fprintf('Train SVM classifier\n');
namesvm=['logs/svm_combine_cifar.mat'];
load('logs/model_cifar-10_2_2_0_2_5_0_100_800_0_1.000000e-01_1.000000e-01_1.000000e-01_1.mat');
model1=model;
load('logs/model_cifar-10_3_0_0_10_0_0_800_0_0_1.000000e-01_1.000000e-01_1.000000e-01_0.mat');
model2=model;
load('logs/model_cifar-10_1_2_0_2_5_0_12_800_0_1.000000e-01_1.000000e-01_1.000000e-01_3.mat');
model3=model;

clear model;
rng(0);
if ~exist(namesvm)
   psiTr=encode(Xtr,model1);
   nrm=(sqrt(sum(psiTr.^2,2)));
   ind=find(nrm);
   psiTr=psiTr(ind,:);
   psiTr=bsxfun(@minus,psiTr,mean(psiTr));
   nrm=mean(sqrt(sum(psiTr.^2)));
   psiTr=psiTr/nrm;
   psiTe=(encode(Xte,model1));
   psiTe=psiTe(ind,:);
   psiTe=psiTe/nrm;
   psiTr2=encode(Xtr,model2);
   nrm=(sqrt(sum(psiTr2.^2,2)));
   ind=find(nrm);
   psiTr2=psiTr2(ind,:);
   psiTr2=bsxfun(@minus,psiTr2,mean(psiTr2));
   nrm=mean(sqrt(sum(psiTr2.^2)));
   psiTr2=psiTr2/nrm;
   psiTe2=(encode(Xte,model2));
   psiTe2=psiTe2(ind,:);
   psiTe2=psiTe2/nrm;
   if exist('model3')
      psiTr3=encode(Xtr,model3);
      nrm=(sqrt(sum(psiTr3.^2,2)));
      ind=find(nrm);
      psiTr3=psiTr3(ind,:);
      psiTr3=bsxfun(@minus,psiTr3,mean(psiTr3));
      nrm=mean(sqrt(sum(psiTr3.^2)));
      psiTr3=psiTr3/nrm;
      psiTe3=(encode(Xte,model3));
      psiTe3=psiTe3(ind,:);
      psiTe3=psiTe3/nrm;
      clear Xte
      clear Xtr;
      psiTr=[psiTr; psiTr2; psiTr3];
      clear psiTr2;
      clear psiTr3;
      psiTe=[psiTe; psiTe2; psiTe3];
      clear psiTe2;
      clear psiTe3;
   else
      clear Xte
      clear Xtr;
      psiTr=[psiTr; psiTr2];
      clear psiTr2;
      psiTe=[psiTe; psiTe2];
      clear psiTe2;
   end
   [acc accVal]=trainSVM(psiTr,Ytr,psiTe,Yte,namesvm);
end
