function [psi] = encode(X,model)
n=size(X,3);

for ii=1:n
   fprintf('%d\r',ii);
   maps=get_zeromap(X(:,:,ii),model.type_zerolayer);
   for jj=1:model.nlayers
      maps=encode_layer(maps,model.layer{jj});
   end
   if ii==1
      ndesc=length(maps(:));
      psi=zeros(ndesc,n);
   end
   psi(:,ii)=maps(:);
end
fprintf('\n');
