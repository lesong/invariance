function [] = main(npatches,subsampling_factors,size_maps,type_zerolayer,dataset, datapath)
if isdeployed
   npatches=str2num(npatches);
   subsampling_factors=str2num(subsampling_factors);
   size_maps=str2num(size_maps);
   type_zerolayer=str2num(type_zerolayer);
end

%%%%%%%% dataset is one of the following values %%%%%%%%
%dataset='mnist';
%dataset='cifar-10';
%dataset='stl-10';

%%%%%%% define the network architecture %%%%%%%
% this is the size of the patches for each layers
%npatches=[5 2 0];  
%subsampling_factors=[2 2 0]; 
%size_maps=[50 200 0];
%type_zerolayer=2; 
                  % 0: extract patches from the image, substracting the mean color (or mean intensity)
                  %    and performing contrast normalization
                  % 1: extract patches from the image, substracting the mean color (or mean intensity)
                  % 2: extract patches from the image, without substracting the mean color (or mean intensity)
                  % 3: subsample orientations from the two-dimensional gradient at every pixel, 
                  %    it requires npatches of the form [1 * *] and a recommended value for size_maps is
                  %    size_maps=[12 * *]  (subsample 12 orientations)
                  
%%%%%%% optional parameters %%%%%%%%
% drives the heuristic to choose the value of sigm
quantiles=[0.1 0.1 0.1];
%%%%%%%% set up the addpaths %%%%%%
 
%%%%%% Initialization %%%%%%%%%%%5
% set the seed to 0
rng(0);
mkdir('logs');
format compact;
param.type_zerolayer=type_zerolayer;
param.npatches=npatches;
param.smaps=size_maps;
param.quantiles=quantiles;
param.subs_factors=subsampling_factors;

name=sprintf('model_%s_%d_%d_%d_%d_%d_%d_%d_%d_%d_%d_%d_%d_%d.mat',dataset,npatches,subsampling_factors,size_maps,quantiles,type_zerolayer);
savename=['logs/' name];

if strcmp(dataset,'mnist')
   [Xtr Ytr Xte Yte]=loadMNIST(datapath);
   param.ntrain=300000; % should be multiple of the training set size
elseif strcmp(dataset,'cifar-10')
   [Xtr Ytr Xte Yte]=loadCIFAR10(datapath);
   param.ntrain=300000; % should be multiple of the training set size
elseif strcmp(dataset,'stl-10')
   [Xtr]=loadSTL10();
   param.ntrain=300000; % should be multiple of the training set size
end

% unsupervised training of the model 
if exist(savename)
   'exists';
   load(savename);
else
   fprintf('Train the network\n');
   model=training(Xtr,param,dataset);
   save(savename,'model');
end

fprintf('Train SVM classifier\n');
namesvm=['logs/svm_' name];
% 3 with normalization
model.ndesc
rng(0);
if ~exist(namesvm)
   if strcmp(dataset,'mnist') || strcmp(dataset,'cifar-10')
      psiTr=encode(Xtr,model);
      clear Xtr;
      psiTr=bsxfun(@minus,psiTr,mean(psiTr));
      nrm=mean(sqrt(sum(psiTr.^2)));
      psiTr=psiTr/nrm;
      psiTe=(encode(Xte,model));
      clear Xte
      psiTe=psiTe/nrm;
      [acc accVal]=trainSVM(psiTr,Ytr,psiTe,Yte,namesvm);
      [tmp ind]=max(accVal);
      ind=find(accVal==tmp);
      fprintf('Classification accuracy is %d\n',acc(ind(end)));
   elseif strcmp(dataset,'stl10')
      acc=cell(1,10);
      accVal=cell(1,10);
      [Xtr Ytr Xte Yte]=loadSTL10Fold(1);
      psiTeO=encode(Xte,model);
      clear Xte;
      clear Xtr;

      for ii=1:10
         [Xtr Ytr Xte Yte]=loadSTL10Fold(ii);
         clear Xte;
         psiTr=encode(Xtr,model);
         clear Xtr;
         psiTr=bsxfun(@minus,psiTr,mean(psiTr));
         nrm=mean(sqrt(sum(psiTr.^2)));
         psiTr=psiTr/nrm;
         psiTe=psiTeO/nrm;
         [acc{ii} accVal{ii}]=trainSVM(psiTr,Ytr,psiTe,Yte);
         save(namesvm,'acc','accVal');
      end
   end
else
   load(namesvm);
   if strcmp(dataset,'mnist') || strcmp(dataset,'cifar-10')
      acc
      accVal
      [tmp ind]=max(accVal);
      ind=find(accVal==tmp);
      fprintf('Classification accuracy is %d\n',acc(ind(end)));
   end
end
