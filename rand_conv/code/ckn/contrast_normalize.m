function X = contrast_normalize(X);
nrm=sqrt(sum(X.^2));
X=bsxfun(@rdivide,X,max(nrm,0.00001));
