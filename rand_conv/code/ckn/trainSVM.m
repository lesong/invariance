function [acc accVal] = trainSVM(Xtr,Ytr,Xte,Yte,name);
if nargin <= 4
   name=[];
end
if (size(Xtr,2) > 20000)
   XteVal=Xtr(:,1:5000);
   YteVal=Ytr(1:5000);
   XtrVal=Xtr(:,5001:end);
   YtrVal=Ytr(5001:end);
   Kfold=false;
else
   Kfold=true;
end
nclasses=max(Ytr)+1;
tabC=2.^(-15:15);
acc=zeros(1,length(tabC));
for ii=1:length(tabC)
   classifier=train(Ytr,Xtr,sprintf('-s 1 -c %d',tabC(ii)),'col');
   [predicted accuracy ddec]=predict(Yte,Xte,classifier,'','col');
   acc(ii)=accuracy(1);

   if Kfold
      accuracy=train(Ytr,Xtr,sprintf('-s 1 -c %d -v 5',tabC(ii)),'col');
   else
      classifier=train(YtrVal,XtrVal,sprintf('-s 1 -c %d',tabC(ii)),'col');
      [predicted accuracy ddec]=predict(YteVal,XteVal,classifier,'','col');
   end
   accVal(ii)=accuracy(1);
   acc
   accVal
   if ~isempty(name)
      save(name,'acc','accVal');
   end
   if ii > 10 && (accVal(ii) < 0.99*max(accVal))
      break;
   end
end


