#!/bin/bash

TOOLS=/home/hanjun/Workspace/C++/caffe/build/tools

$TOOLS/caffe train --solver=markovnet_solver.prototxt \
    --snapshot=result_iter_700.solverstate
