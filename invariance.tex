\documentclass{article}

\usepackage{graphicx} % more modern
\usepackage{subfigure}
\usepackage{xspace}
\usepackage{caption}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

\usepackage{enumitem}
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usepackage{fullpage}
\usepackage{authblk}

% other packages
\usepackage{Definitions}
\usepackage{color}

\renewcommand{\leq}{\leqslant}
\renewcommand{\le}{\leqslant}
\renewcommand{\geq}{\geqslant}
\renewcommand{\ge}{\geqslant}


%\newcommand{\span}{\textrm{span}}

\newcommand{\Le}[1]{{\color{red} [Le: #1]}}
\newcommand{\BoX}[1]{{\color{blue} [BoX: #1]}}
\newcommand{\Zqian}[1]{{\color{cyan} [Zhiqian: #1]}}
\newcommand{\bruce}[1]{{\color{cyan} [Bruce: #1]}}

\setlength\parindent{0pt}
\setlength{\parskip}{1em}

\title{\huge Nonparametric Learning with Invariance}

\author{XXX}

\begin{document}
\maketitle

\begin{abstract}
\end{abstract}

\section{Reproducing Partial Derivatives in RKHS}

A \emph{reproducing kernel Hilbert space (RKHS)} $\Hcal$ on $\Xcal$ with a kernel $k(x,x')$ is a Hilbert space of
functions $f(\cdot):\Xcal \mapsto \RR$ with inner product $\inner{\cdot}{\cdot}_{\Hcal}$. Its element $k(x,\cdot)$ satisfies the reproducing property:
$$
  \inner{f(\cdot)}{k(x, \cdot)}_{\Hcal} = f(x),
$$
and consequently, $\inner{k(x,\cdot)}{k(x', \cdot)}_{\Hcal} = k(x,x')$,
meaning that we can view the evaluation of a function $f$ at any point $x\in\Xcal$ as an inner product. Alternatively, $k(x,\cdot)$ can  be viewed as an implicit feature map $\phi(x)$ where $k(x,x')=\inner{\phi(x)}{\phi(x')}_{\Hcal}$.
For instance, when $\Xcal=\RR^d$, the \emph{normalized} Gaussian RBF kernel is defined as
\begin{align}
k(x,x') = \exp(- \nbr{x-x'}^2 / (2 s^2)) / (\sqrt{2\pi} s^d). \label{eq:normalizedrbf}
\end{align}
Kernel functions have also been defined on
graphs, time series, dynamical systems, images and other structured
objects~\cite{SchTsuVer04}.

For a function $f(x)$ of $d$ variable $x=(x_1,\ldots,x_d)^\top \in \Xcal=\RR^d$, we denote its partial derivative $D f$ at $x$ as
\begin{align}
  D f(x) =
  \rbr{
    \begin{matrix}
      D_1 f(x) \cr
      \vdots \cr
      D_d f(x)
    \end{matrix}
  }
  :=
  \rbr{
    \begin{matrix}
      \frac{\partial}{\partial x_1} f(x) \cr
      \vdots \cr
      \frac{\partial}{\partial x_d} f(x)
    \end{matrix}
  }
\end{align}
For a kernel $k(x,x') \in C^2(\Xcal\times\Xcal)$, we denote its partial derivative at $x$ as
\begin{align}
  D k(x,\cdot) =
  \rbr{
    \begin{matrix}
      D_1 k(x,\cdot)^\top \cr
      \vdots \cr
      D_d k(x,\cdot)^\top
    \end{matrix}
  }
  :=
  \rbr{
    \begin{matrix}
      \frac{\partial}{\partial x_1} k(x,\cdot)^\top \cr
      \vdots \cr
      \frac{\partial}{\partial x_d} k(x,\cdot)^\top
    \end{matrix}
  }
\end{align}
We note that partial derivative operator $D$ is a linear operator.

\begin{theorem}[Reproducing Partial Derivatives~\cite{Zhou08}]
  Let $k(x,x'):\Xcal\times \Xcal \mapsto \RR$ be a PSD kernel with RKHS $\Hcal$ such that $k(x,x') \in C^2(X\times X)$.
  Then
  \begin{enumerate}
    \item For any $x \in \Xcal$, $D_ik(x,\cdot) \in \Hcal$.
    \item For any $x \in \Xcal$, $f \in \Hcal$, $D_if(x) = \inner{f}{D_ik(x,\cdot)}_{\Hcal}$.
  \end{enumerate}
\end{theorem}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We want to take into account invariance in supervised nonparametric learning algorithms. Denote a data point by $x$ with domain $\Xcal$ and its transformed version by $x(\theta)$ with parameter $\theta$.

\paragraph{Examples.} A grey-scale image $x$ can be considered as a function in 2D plane
\begin{align}
  x(z_1,z_2):\RR\times\RR \mapsto \RR
\end{align}
where $z_1$ and $z_2$ are the two dimensional coordinates for a pixel.
Then we can transform image $x$ into $x(\theta)$ using
\begin{align}
  &\text{Translation:}~&&x(\theta)(z_1,z_2) = x(z_1-\theta_1, z_2-\theta_2) \\
  &\text{Rotation:}~&&x(\theta)(z_1,z_2) = x(z_1 \cos(\theta) - z_2 \sin(\theta), z_2 \sin(\theta) + z_1 \cos(\theta)) \\
  &\text{Scaling:}~&&x(\theta)(z_1,z_2) = x(z_1 + \theta z_1, z_2 + \theta z_2)
\end{align}
Very often, we just sample the image $x(z_1,z_2)$ in a discrete set of $d$ locations $\cbr{(z_1^i,z_2^i)}_{i=1}^d$. In this case, we may denote $x(\theta)$ as a $d$-dimensional vector $x(\theta) = (x_1(\theta),\ldots,x_d(\theta))^\top$ where each dimension corresponding to a sampled pixel.


% \begin{itemize}
%   \item Translation.
%     \begin{align}
%       x(\theta)(z_1,z_2) = x(z_1-\theta, z_2-\theta)
%     \end{align}
%   \item Rotation.
%     \begin{align}
%       x(\theta)(z_1,z_2) = x(z_1 \cos(\theta) - z_2 \sin(\theta), z_2 \sin(\theta) + z_1 \cos(\theta))
%     \end{align}
%   \item Scaling.
%     \begin{align}
%       x(\theta)(z_1,z_2) = x(z_1 + \theta z_1, z_2 + \theta z_2)
%     \end{align}
% \end{itemize}
% Given a dataset of $m$ training image $x_i$ and label $y_i$ pairs
% \begin{align}
%   \Dcal = \cbr{(x_1,y_1),\ldots,(x_m,y_m)},
% \end{align}
% the goal is to learn a function

A function $f:\Xcal\mapsto \Ycal$ is invariant to transformation parameterized by $\theta \in \Theta$ if
\begin{align}
  f(x) = f(x(\theta)),\quad \forall\, \theta \in \Theta.
\end{align}
This is equivalent to requiring
\begin{align}
  D_{\theta} f(x(\theta)) := \frac{\partial f(x(\theta))}{\partial \theta} = \frac{\partial x(\theta)}{\partial \theta} \circ \frac{\partial f(x(\theta))}{\partial x(\theta)}  = 0,
\end{align}
where
\begin{itemize}
  \item $D f(x(\theta)):=\frac{\partial f(x(\theta))}{\partial x(\theta)}$ is the gradient of $f$ with respect to $x(\theta)$ (a vector);
  \item $D_{\theta} x(\theta):=\frac{\partial x(\theta)}{\partial \theta}$ is the gradient of $x(\theta)$ with respect to parameter vector $\theta$ (an operator);
  \item $D_{\theta} x(\theta)\circ D f(x(\theta)):=\frac{\partial x(\theta)}{\partial \theta} \circ \frac{\partial f(x(\theta))}{\partial x(\theta)}$ means applying operator $\frac{\partial x(\theta)}{\partial \theta}$ on vector $\frac{\partial f(x(\theta))}{\partial x(\theta)}$.
\end{itemize}

\section{Measuring Invariance}

Define some norm $\nbr{\cdot}$ of $D_{\theta} f(x(\theta))$ to measure its magnitude. For instance,
\begin{align}
  &\text{$\ell_{\infty}$ norm:}~&& \nbr{D_{\theta} f(x(\theta))}_{\infty} := \max_{\theta \in \Theta} \nbr{D_{\theta} f(x(\theta))} \\
  &\text{$\ell_{2}$ norm:}~&& \nbr{D_{\theta} f(x(\theta))}_{2} := \rbr{\int_{\Theta} \nbr{D_{\theta} f(x(\theta))}^2 d\PP(\theta)}^{1/2}
\end{align}
Suppose function $f$ is in the RKHS $\Hcal$ associated with kernel $k(x,x')$, we can use the derivative reproducing property and obtain
\begin{align}
  D_if(x(\theta)) = \inner{f}{D_i k(x(\theta),\cdot)}_{\Hcal}
\end{align}
Then
\begin{align}
  D f(x(\theta)) = \rbr{
    \begin{matrix}
      \inner{f}{D_1 k(x(\theta),\cdot)}_{\Hcal} \cr
      \vdots \cr
      \inner{f}{D_d k(x(\theta),\cdot)}_{\Hcal}
    \end{matrix}
  } = \rbr{
    \begin{matrix}
      D_1 k(x(\theta),\cdot)^\top  \cr
      \vdots \cr
      D_d k(x(\theta),\cdot)^\top
    \end{matrix}
  }
  f
  = D  k(x(\theta),\cdot) f
\end{align}
Furthermore,
\begin{align}
  D_{\theta} x(\theta) = \rbr{
      D_{\theta}x_1(\theta), \ldots, D_{\theta}x_d(\theta)
  }
\end{align}
Then the gradient is a linear function of $f$:
\begin{align}
  D_{\theta} f(x(\theta)) = D_{\theta} x(\theta)\circ D f(x(\theta))
  &= \rbr{\sum_{i = 1}^d D_{\theta}x_i(\theta) D_i k(x(\theta),\cdot)^\top } f \\
  &= D_{\theta} x(\theta) D  k(x(\theta),\cdot) f \\
  &= \inner{\psi_{x(\theta)}}{f}_{\Hcal} \label{eqn:derivative}
\end{align}
where $\psi_{x(\theta)}:=D_{\theta} x(\theta) D  k(x(\theta),\cdot) \in \Hcal$.
and the squared form is:
\begin{align}
  \nbr{D_{\theta} x(\theta)\circ D f(x(\theta))}^2
  &= f^\top \rbr{\psi_{x(\theta)} \psi_{x(\theta)}^\top} f
\end{align}
Suppose the orbit of $x$ in the RKHS be
\begin{align}
  \Psi_x := \cbr{\psi_{x(\theta)} |\, \theta \in \Theta}
\end{align}
Essentially, we want $f$ to vanish (or very small) in the entire orbit,~\ie,
\begin{align}
  \inner{\psi_{x(\theta)}}{f}_{\Hcal} = 0, \quad \forall \psi_{x(\theta)} \in \Psi_x
\end{align}

\Le{Notation seems clumsy}

\section{Optimization}

Given a dataset of $m$ training image $x^i$ and label $y^i$ pairs
\begin{align}
  \Dcal = \cbr{(x^1,y^1),\ldots,(x^m,y^m)},
\end{align}
the goal is to learn a function $f$ that minimizes the following objective
\begin{align}
  \frac{1}{m} \sum_{i=1}^m \ell(x^i, y^i) + \lambda \rbr{ \frac{1}{m} \sum_{i=1}^m \nbr{D_{\theta} f(x^i(\theta))} + \tau \nbr{f}^2} \label{eqn:opt}
\end{align}
where $\ell$ is a loss function convex in $f$. One can prove a representer theorem for this optimization problem
\begin{align}
  f(x) = \sum_{i=1}^m \rbr{\alpha_i k(x^i,x) + \sum_{\theta \in \Theta} \sum_{j=1}^d \beta_{ij\theta} D_j k(x^i(\theta),x)}
\end{align}
One can use conditional gradient or SGD for optimization. It is interesting that the representer theorem looks like generating virtual examples.


\section{Reduction in Rademacher Complexity}

Consider the following optimization. \bruce{Note that the objective is not the same as (\ref{eqn:opt}). The regularization term on the derivatives are squared. The later analysis implies that this may be more appropriate.}
\begin{align}
 \min_f ~~\frac{1}{m} \sum_{i=1}^m \ell(x^i, y^i) + \lambda \rbr{ \frac{1}{m} \sum_{i=1}^m \nbr{D_{\theta} f(x^i(\theta))}^2 + \tau \nbr{f}_\Hcal^2} \label{eqn:opt2}
\end{align}
By (\ref{eqn:derivative}), and assuming a discrete transformation parameter set with uniform distribution, we have
\[
  \frac{1}{m} \sum_{i=1}^m \nbr{D_{\theta} f(x^i(\theta))}^2 = \frac{1}{m |\Theta|} \sum_{i=1}^m \sum_{\theta \in \Theta}\inner{\psi_{x^i(\theta)}}{f}_{\Hcal}^2. 
\]
View $\psi_{x^i(\theta)}$ as the feature map of a virtual example $x_\textnormal{vir}(i,\theta)$. For clarity, denote the set of virtual examples as $\cbr{x^i}_{i=m+1}^{m+m'}$. Then the optimization is 
\begin{align}
 \min_f~~\frac{1}{m} \sum_{i=1}^m \ell(x^i, y^i) + \gamma \sum_{i=m+1}^{m+m'}  \inner{\phi(x^i)}{f}_{\Hcal}^2 + \mu \inner{f}{f}_{\Hcal} \label{eqn:opt3}
\end{align}
where $ \gamma = \lambda/m|\Theta|$ and $\mu=\tau\lambda$.
By the representer theorem,
\[
  f = \sum_{i=1}^{m+m'} \alpha_i \phi(x^i)
\]
where $\phi$ is the feature map. 

Consider the following equivalent optimization problem
\begin{align}
 \min_f~~& \frac{1}{m} \sum_{i=1}^m \ell(x^i, y^i) \\
 \textnormal{subject to~~} & \gamma \sum_{i=m+1}^{m+m'}  \inner{\phi(x^i)}{f}_{\Hcal}^2 + \mu \inner{f}{f}_{\Hcal} \leq \Lambda^2\label{eqn:opt4}
\end{align}
for some $\Lambda \geq 0$.
Then we need to bound the Rademacher complexity of the hypothesis class $\Hcal_\Lambda$ on $\Scal = \cbr{x^i: 1\leq i\leq m}$ where
\begin{align}
\Hcal_\Lambda := \cbr{f: f = \sum_{i=1}^{m+m'} \alpha_i \phi(x^i) \textnormal{~and~} \gamma \sum_{i=m+1}^{m+m'}  \inner{\phi(x^i)}{f}_{\Hcal}^2 + \mu \inner{f}{f}_{\Hcal} \leq \Lambda^2}. \label{eqn:hyp}
\end{align}
For the analysis, we denote the kernel matrix as $A = \sbr{\inner{\phi(x^i)}{\phi(x^j)}_{\Hcal} }_{i,j=1}^{m}$ and 
	the extended kernel matrix as $K = \sbr{\inner{\phi(x^i)}{\phi(x^j)}_{\Hcal} }_{i,j=1}^{m+m'} $, so that
\begin{align}
K=
	\begin{bmatrix}
	A & C\\
	C^\top & B
	\end{bmatrix}.
\end{align}
Then $\inner{f}{f}_{\Hcal} = \alpha^\top K \alpha$ and 
\[
  \sum_{i=m+1}^{m+m'}  \inner{\phi(x^i)}{f}_{\Hcal}^2 
	= \alpha^\top 
	\begin{bmatrix}
	C\\ B
	\end{bmatrix}
	\begin{bmatrix}
	C^\top & B
	\end{bmatrix}
	\alpha
	= \alpha^\top G \alpha,
	\textnormal{~~where~} G:= 
	\begin{bmatrix}
	C\\ B
	\end{bmatrix}
	\begin{bmatrix}
	C^\top & B
	\end{bmatrix}.
\]

\begin{theorem}
Assuming all matrix inversions are meaningful. 
\[
  \widehat{\Rcal}_m( \Hcal_\Lambda) \leq \frac{\Lambda}{m} \sqrt{\frac{1}{\mu}\tr\rbr{A} - \frac{\gamma}{\mu^2}\tr\rbr{C\rbr{I + \frac{\gamma}{\mu} B}^{-1} C^\top}}.
\]
\end{theorem}

\begin{proof}
Let $F = (\gamma G + \mu K)^{-1/2}$. 
\begin{align*}
\widehat{\Rcal}_m( \Hcal_\Lambda) & = \frac{1}{m} \EE_\sigma\cbr{\sup_{\alpha} \cbr{\sigma^\top \sbr{A~~C} \alpha : \alpha^\top (\gamma G + \mu K) \alpha \leq \Lambda^2 }} \\
& = \frac{1}{m} \EE_\sigma\cbr{\sup_{\alpha} \cbr{\sigma^\top \sbr{A~~C}F F^{-1}\alpha : \rbr{F^{-1}\alpha }^\top F^{-1} \alpha \leq \Lambda^2 }} \\
& \leq \frac{\Lambda}{m} \EE_\sigma\cbr{ \nbr{\sigma^\top \sbr{A~~C}F}_2 } && \text{(Cauchy-Schwartz)}\\
& \leq \frac{\Lambda}{m} \sqrt{\EE_\sigma\cbr{ \nbr{\sigma^\top \sbr{A~~C}F}^2_2 } } && \text{(Jensen's)}\\
& = \frac{\Lambda}{m} \sqrt{\EE_\sigma\cbr{ \sigma^\top \sbr{A~~C}F F^\top \begin{bmatrix}A \\ C^\top\end{bmatrix}\sigma} } \\
& = \frac{\Lambda}{m} \sqrt{\tr\cbr{ \sbr{A~~C}F F^\top \begin{bmatrix}A \\ C^\top\end{bmatrix}} } && \text{(Rademacher variables)}\\
& = \frac{\Lambda}{m} \sqrt{\tr\cbr{ \sbr{A~~C} (\gamma G + \mu K)^{-1} \begin{bmatrix}A \\ C^\top\end{bmatrix}} }.
\end{align*}
By the matrix inversion lemma (Sherman--Morrison--Woodbury formula),  
\begin{align*}
(\gamma G + \mu K)^{-1} & =(\mu K + \gamma 
	\begin{bmatrix}
	C\\ B
	\end{bmatrix}
	\begin{bmatrix}
	C^\top & B
	\end{bmatrix}
	)^{-1} \\
& = (\mu K)^{-1} - \gamma (\mu K)^{-1} 
	\begin{bmatrix}
	C\\ B
	\end{bmatrix} 
	\cbr{I + \gamma\begin{bmatrix} 	C^\top & B	\end{bmatrix} (\mu K)^{-1} \begin{bmatrix} 	C \\ B	\end{bmatrix} }^{-1} 
	\begin{bmatrix}
	C^\top & B
	\end{bmatrix} 
	(\mu K)^{-1}
\end{align*}
and using block matrix inversion by LDU decomposition
\begin{align*}
K^{-1} & = 
\begin{bmatrix}
	I & 0 \\
	- B^{-1} C^\top & I
	\end{bmatrix} 
\begin{bmatrix}
	(A - C B^{-1} C^\top)^{-1} & 0 \\
	0 & B^{-1}
	\end{bmatrix} 
\begin{bmatrix}
	I & - C B^{-1}\\
	 0 & I
	\end{bmatrix} . 
\end{align*}
It  can be verified that 
\begin{align*}
\begin{bmatrix} 	A & C	\end{bmatrix} 	K^{-1} = \begin{bmatrix} 	I & 0	\end{bmatrix}, \\
\begin{bmatrix} 	A & C	\end{bmatrix} 	K^{-1} \begin{bmatrix} 	A \\ C^\top \end{bmatrix} = A, \\
\begin{bmatrix} 	C^\top & B	\end{bmatrix} 	K^{-1} \begin{bmatrix} 	C \\ B \end{bmatrix} = B.
\end{align*}
Then 
\begin{align*}
\tr\cbr{ \begin{bmatrix}A & C \end{bmatrix} (\gamma G + \mu K)^{-1} \begin{bmatrix}A \\ C^\top\end{bmatrix} } & 
= \frac{1}{\mu}\tr\rbr{A} -  \tr\rbr{ \frac{\gamma}{\mu^2}
 \begin{bmatrix}
	I & 0
	\end{bmatrix} 
	\begin{bmatrix}
	C\\ B
	\end{bmatrix} 
	\rbr{I + \frac{\gamma}{\mu} B }^{-1} 
	\begin{bmatrix}
	C^\top & B
	\end{bmatrix} 
	\begin{bmatrix}
	I \\ 0
	\end{bmatrix} 
	 } \\
	& = \frac{1}{\mu}\tr\rbr{A} -  \frac{\gamma}{\mu^2}\tr\rbr{  C
	\rbr{I + \frac{\gamma}{\mu} B }^{-1} 	C^\top 
	 }
\end{align*}
completing the proof.
\end{proof}
When there is no invariance regularization ($\gamma = 0$), the Rademacher complexity bound reduces to
\[ 
 \widehat{\Rcal}_m( \Hcal) \leq \frac{\Lambda}{m} \sqrt{\frac{1}{\mu}\tr\rbr{A} }. 
\]
So there is a reduction with the invariance regularization and the reduction is significant when
\begin{align}
  \frac{\gamma}{\mu}\tr\rbr{  C 	\rbr{I + \frac{\gamma}{\mu} B }^{-1} 	C^\top }
\end{align}
is large compared to $\tr(A)$.
This term depends on the virtual examples, which in turn depends on the type of invariance.

\subsection{Interpretation of the Reduction}
The reduction is $\Delta(\gamma/\mu)$ where 
\begin{align}
  \Delta(\eta) := \eta\tr\rbr{  C 	\rbr{I + \eta B }^{-1} 	C^\top }\label{eqn:reduction}
\end{align}
Clearly, we have $\Delta(0) = 0$. Furthermore, $\Delta(\eta)$ is non-decreasing and 
\[
  \lim_{\eta \rightarrow \infty} \Delta(\eta) = C 	B^{-1} 	C^\top.
\]
This is the reduction we get when we require  the learned hypothesis to be strictly invariant.

Suppose that the transformation parameter set is discrete, and the derivatives are just discrete derivatives with step size $\delta$. Suppose $x^{m+j} = x_\textnormal{vir}(i',\theta)$, then by the definition of $C$,
\begin{align*}
	C_{ij} = \inner{\phi(x^i)}{\psi_{x^{i'}(\theta)}}_{\Hcal} = \inner{\phi(x^i)}{\psi_{x^{i'}(\theta)}}_{\Hcal} 
	&= \sum_{\ell=1}^d \frac{k(x^i, x^{i'}(\theta)) - k(x^i, x^{i'}(\theta+\delta e_\ell ))}{\delta} \\
	& = \sum_{\ell=1}^d \frac{k(x^i, x^{i'}(\theta))}{\delta} - \sum_{\ell=1}^d \frac{k(x^i, x^{i'}(\theta+\delta e_\ell ))}{\delta}  \\
  & =: \Delta k(x^i, x^{i'}(\theta))
\end{align*}
which is just the normalized fluctuation of the kernel value between $x^i$ and $x^{m+j}$.  So $C_{i\cdot}$ is just the fluctuations of the kernel values of $x^i$ on all the virtual examples. Since $B$ is PSD,
\begin{align*}
  \Delta(\eta) & = \tr\rbr{  C 	\rbr{I/\eta +  B }^{-1} 	C^\top } = \sum_{i=1}^m \nbr{C_{i\cdot}}_{\rbr{I/\eta +  B }^{-1}}
\end{align*}
measures the total fluctuation of the kernel on the training examples. The fluctuation is normalized by $\rbr{I/\eta +  B }^{-1}$, which is close to Euclidean distance when $\eta$ is small. Therefore, if the kernel value is significantly not invariant to the transformation, then we get a significant reduction in the complexity. This is intuitive, since in the extreme case when the kernel is invariant to the transformation and thus all hypotheses is not penalized, we cannot expect any complexity reduction.

The definition of $\psi_{x^{i'}(\theta)}$ also implies that the invariance regularization term is equivalent to the following: first generate the virtual examples $x^{i'}(\theta)$ and $x^{i'}(\theta+\delta e_\ell)$, and then penalize the difference of the function values $f(x^{i'}(\theta))$ and $f(x^{i'}(\theta+\delta e_\ell))$.

%\paragraph{Remark 1}  To see a concrete example of the reduction (\ref{eqn:reduction}), we may consider the extreme case when
%the kernel value is invariant to the transformation, \ie,
%\[
  %k(x^i(\theta), x) = k(x^i, x) 
%\]
%for any $x^i, x$ and $\theta$.

%\paragraph{Remark 2} 
%The analysis assumes a discrete transformation parameter set with uniform distribution, and assumes the virtual examples.
%We should be able to provide an analysis for continuous transformation parameter set with non-uniform distribution, since there is no inherent difficulty in doing so. But I am not sure about the existence of the virtual examples, \ie,
%there exists $x_\textnormal{vir}^i(\theta) \in \Xcal$ such that $\phi(x_\textnormal{vir}^i(\theta)) = \psi_{x^i(\theta)}$.
%One possible way to get around this is to work directly with $\psi_{x^i(\theta)}$. 
%More precisely, assume there exists a linear operator $\Gamma$ on the RKHS such that the inversion $\Gamma^{-1}$ exists and
%\[
  %\inner{\Gamma^{-1} f}{\Gamma^{-1} f}_{\Hcal} = \gamma \sum_{i=1}^m \sum_{\theta \in \Theta}\inner{\psi_{x^i(\theta)}}{f}_{\Hcal}^2 + \mu \inner{f}{f}_{\Hcal}. 
%\]
%Then
%\begin{align*}
%\widehat{\Rcal}_m( \Hcal_\Lambda) & = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{ \sum_{i=1}^m \sigma_i \inner{f}{\phi(x^i)}_\Hcal : \inner{\Gamma^{-1} f}{\Gamma^{-1} f}_{\Hcal} \leq \Lambda^2 }} \\
%& = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{  \inner{f}{\sigma^\top \Phi}_\Hcal : \inner{\Gamma^{-1} f}{\Gamma^{-1} f}_{\Hcal} \leq \Lambda^2 }} &&  (\Phi := \begin{bmatrix} \phi(x^1)~ \phi(x^2)~ \cdots~\phi(x^m) \end{bmatrix}^\top)\\
%& = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{  \inner{\Gamma \Gamma^{-1}f}{\sigma^\top \Phi}_\Hcal : \inner{\Gamma^{-1} f}{\Gamma^{-1} f}_{\Hcal} \leq \Lambda^2 }} && \text{(inversion)}\\
%& = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{  \inner{\Gamma^{-1}f}{\Gamma^* \sigma^\top \Phi}_\Hcal : \inner{\Gamma^{-1} f}{\Gamma^{-1} f}_{\Hcal} \leq \Lambda^2 }} && \text{($\Gamma^*$ is the adjoint operator)}\\
%& \leq \frac{\Lambda}{m} \EE_\sigma\cbr{  \nbr{\Gamma^* \sigma^\top \Phi}_\Hcal}. && \text{(Cauchy-Schwartz)}
%\end{align*}
%We need to show that 
%\[
 %\Gamma^* \Phi = \begin{bmatrix} \Gamma^* \phi(x^1) \\ \Gamma^* \phi(x^2) \\ \vdots \\ \Gamma^* \phi(x^m) \end{bmatrix} 
%=  \begin{bmatrix} A & C \end{bmatrix} \rbr{\gamma G + \mu K}^{-1/2}.
%\]

\subsection{Analysis for General Transformation Parameter Set}

\bruce{The proof in this section may not be strictly right, as I am new to the Green's function method and not familiar with the operator theory. Especially, the assumptions needed for the proof are not clear to me. It will be nice if anyone can point me to some reference.

The bound here include the one for the finite transformation parameter set as a special case. But it is still a bit difficult to parse. }

For simplicity of notations, let $w = [i; \theta] \in [m] \otimes \Theta$ and $x_w = x^i(\theta)$. Let $\EE_i g(x^i) = \frac{1}{m}\sum_{i=1}^m g(x^i) $ denote the average of the function $g$ over examples, $\EE_w g(x_w) = \frac{1}{m} \sum_{i=1}^m\int_\theta g(x^i(\theta)) d\PP(\theta)$ denote the average over examples and transformations.

Note that $\EE_w \sbr{ \inner{\psi_{x_w}}{f}_\Hcal^2 } = f^\top \sbr{\EE_w\rbr{\psi_{x_w} \psi_{x_w}^\top}} f$,
and $\inner{f}{f}_\Hcal = f^\top f$.
Then the optimization can be written as
\begin{align}
 \min_f~~& \EE_i \sbr{\ell(x^i, y^i) }\\
 \textnormal{subject to~~} & f^\top \Psi f \leq \Lambda^2, \textnormal{ where } \Psi := \lambda\EE_w\rbr{\psi_{x_w} \psi_{x_w}^\top} + \mu I.
\end{align}

To analyze the reduction of complexity, 
let 
\[
A_{ij} = \inner{\phi(x^i)}{\phi(x^j)}, B(w,w') = \inner{\psi_{x_w}}{\psi_{x_w'}}, C(i, w) = \inner{\phi(x^i)}{\psi_{x_w}}.
\]
Define a linear operator $\BB$ on functions over $w$ as $(\BB g)(w)= \EE_w' \sbr{ g(w') B(w,w')}$.

Assume $\Psi^{1/2}$ and $\Psi^{-1/2}$ exist, and
assume $\BB$ has complete eigenfunctions $F_\ell$ and corresponding eigenvalues $\lambda_\ell (\ell=1,2,\dots)$. 
Note that $F_\ell$ are real.

\begin{theorem}
\[
  \widehat{\Rcal}_m( \Hcal_\Lambda)  \leq \frac{\Lambda}{m} \sqrt{\frac{1}{\mu} \sum_{i=1}^m A_{ii} - \frac{1}{\mu} \sum_{i=1}^m\sum_{\ell=1}^{\infty} \frac{1}{\lambda_\ell + \mu/\lambda}\EE_w \sbr{ F_\ell(w) C(i,w) }^2}. 
\]
\end{theorem}

\begin{proof}
\begin{align*}
\widehat{\Rcal}_m( \Hcal_\Lambda) 
& = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{  \inner{f}{\sigma^\top \Phi}_\Hcal : f^\top \Psi f \leq \Lambda^2 }} &&  (\Phi := \begin{bmatrix} \phi(x^1)~ \phi(x^2)~ \cdots~\phi(x^m) \end{bmatrix}^\top)\\
& = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{  \inner{\Gamma \Gamma^{-1}f}{\sigma^\top \Phi}_\Hcal : f^\top \Psi f \leq \Lambda^2 }} \\
& = \frac{1}{m} \EE_\sigma\cbr{\sup_{f} \cbr{  \inner{\Psi^{1/2}f}{\Psi^{-1/2} (\sigma^\top \Phi)}_\Hcal : f^\top \Psi f \leq \Lambda^2 }} && \text{(assuming $\Psi^{1/2}$ and $\Psi^{-1/2}$ exist)}\\
& \leq \frac{\Lambda}{m} \EE_\sigma\cbr{  \nbr{\Psi^{-1/2} (\sigma^\top \Phi)}_\Hcal} && \text{(Cauchy-Schwartz)} \\
& \leq \frac{\Lambda}{m}  \sqrt{ \EE_\sigma\cbr{ (\sigma^\top \Phi)^\top \Psi^{-1} (\sigma^\top \Phi) }} && \text{(Jensen's)} \\
& = \frac{\Lambda}{m} \sqrt{\sum_{i=1}^m   \phi(x^i)^\top \Psi^{-1} \phi(x^i)}.  && \text{(Rademacher variables)} \\
& = \frac{\Lambda}{m} \sqrt{\frac{1}{\mu} \sum_{i=1}^m A_{ii} - \frac{1}{\mu} \sum_{i=1}^m\sum_{\ell=1}^{\infty} \frac{1}{\lambda_\ell + \mu/\lambda}\EE_w \sbr{ F_\ell(w) C(i,w) }^2}. && \text{(Lemma~\ref{lem:term} proved below)} 
\end{align*}
\end{proof}

\begin{lemma}\label{lem:rep}
$\Psi^{-1} \phi(x^i) \in \textnormal{span}(\cbr{ \phi(x^i), \psi(x^i(\theta)): i\in [m], \theta\in \Theta } )$.
\end{lemma}
\begin{proof}
Let $\Hcal_0 = \textnormal{span}(\cbr{ \phi(x^i), \psi(x^i(\theta)): i\in [m], \theta\in \Theta } )$.  
Suppose $\Psi^{-1} \phi(x^i) = g_1 + g_2$ where $g_1 \in \Hcal_0$ and $g_2 \in \Hcal_0^\bot$. 
Then 
\[
\phi(x^i) = \Psi (g_1+g_2) = \Psi g_1 + \lambda\EE_w\rbr{\psi_{x_w} \inner{\psi_{x_w}}{g_2} } + \mu g_2 = \Psi g_1 + \mu g_2.
\]
Since $\phi(x^i)$  and $ \Psi g_1$  are in $\Hcal_0$, then $\mu g_2 \in \Hcal_0$. This means $g_2 = 0$, completing the proof.
\end{proof}

\begin{lemma}\label{lem:term}
\begin{align}
\phi(x^i)^\top \Psi^{-1} \phi(x^i)  = \frac{1}{\mu} A_{ii} - \frac{1}{\mu} \sum_{\ell=1}^{\infty} \frac{1}{\lambda_\ell + \mu/\lambda}\EE_w \sbr{ F_\ell(w) C(i,w) }^2 . 
\end{align}
\end{lemma}
\begin{proof}
Let $\phi$ be short for $\phi(x^i)$.
By Lemma~\ref{lem:rep}, we can write 
\[
\Psi^{-1} \phi = \EE_j \sbr{a_j \phi(x^j)} + \EE_w \sbr{ b(w) \psi_{x_w}}.
\]
Then we have
\begin{align*}
\phi = \EE_j \sbr{a_j \Psi\phi(x^j)} + \EE_w \sbr{ b(w) \Psi\psi_{x_w}}
\end{align*}
where 
\begin{align*}
\Psi\phi & = \lambda \EE_w' \sbr{ \psi_{x_w'} C(i,w')} + \mu \phi,\\
\Psi\psi_{x_w} & = \lambda \EE_w' \sbr{ \psi_{x_w'} B(w',w)} + \mu \psi_{x_w}.
\end{align*}
It is easy to see that $a_i = 1/\mu$ and $a_j = 0 (j\neq i)$, and $b$ satisfies
\begin{align*}
-\mu \EE_w' \sbr{ b(w') \rbr{ B(w,w') + \frac{\mu}{\lambda} \delta(w-w')} } = C(i,w)
\end{align*}
where $\delta(w-w')$ is the Dirac delta function with $\EE_w'\sbr{ g(w') \delta(w-w')} =  g(w)$.
Then $b$ can be calculated by the method of Green's function. More precisely,
define a linear operator 
\[
\LL b = -\mu  \EE_w' \sbr{ b(w') \rbr{ B(w,w') + \frac{\mu}{\lambda} \delta(w-w')} }.
\]
Then $\LL$ has eigenfunctions $F_\ell$ with corresponding eigenvalues $-\mu(\lambda_\ell + \mu/\lambda)$,
so its Green's function is
\begin{align}
G(w,w') = \sum_{\ell=1}^{\infty} \frac{F^\dagger_\ell(w) F_\ell(w')}{-\mu(\lambda_\ell + \mu/\lambda)}, \label{eqn:green}
\end{align}
and $b$ can be computed as $b(w) = \EE_w' \sbr{ G(w,w') C(i,w')} $.
The lemma then follows from 
\[
\phi(x^i)^\top \Psi^{-1} \phi(x^i) = \EE_j \sbr{a_j A_{ij}} + \EE_w \sbr{b(w) C(i,w)}
\]
and plugging in the expression of $a, b$ and (\ref{eqn:green}).
\end{proof}

\Le{A few things to think: 
\begin{itemize}
  \item Population Rademacher complexity. will relate to the eigenvalues of the integral operator defined by the kernel. For the second term $C B^{-1} C^\top$ term, we essentially define a new kernel using those RKHS elements related to derivatives of the transformed examples. How is the spectrum of the RKHS being modified. 
  \item Will one type of invariance be more difficult than another (eg. translation vs. scaling). 
  How is the amount of reduction in complexity related to the type of invariance?
  \item Does it suggest whether one kernel is better than another? Does it help us to define better kernels (see connection to regularization operator paper Smola et al. 98)? Analysis of particular kernel?
  \item Will we still get a nice expression if we do not discretize the $\theta$ space? 
  \item Other norms? which q-norm is better?
	\item Possible connection to Nystrom's method? Regularization by unlabeled data?
	\item Regularization by $\EE_\theta\sbr{f(x) - f(x(\theta))}^2$?
\end{itemize}

}

\section{Learning Invariance Kernels}

We want to learn the kernel function $k(x,x')$ such that function $f$ in the span of $\cbr{k(x,\cdot), x\in \Xcal}$ is more invariant to some transformation. That is 
\begin{align}
  D_{\theta} f(x(\theta)) = \inner{\psi_{x(\theta)}}{f}_{\Hcal}
\end{align}
is small (remember that $\psi_{x(\theta)}:=D_{\theta}x(\theta)Dk(x(\theta),\cdot)$).  
One way to learn such a kernel function is like this: 
\begin{align}
  k^*(x,x') = \argmin_{k} \sum_{x}\sum_{\theta} \nbr{\psi_{x(\theta)}}_{\Hcal}^2 = \sum_{x}\sum_{\theta} D_{\theta}x(\theta) D^2 k(x(\theta),x(\theta)) (D_{\theta}x(\theta))^\top 
\end{align}
Suppose that our kernel function has a random feature representation as 
\begin{align}
  k(x,x') = \int_{\Omega} \phi_{\omega}(x)\phi_{\omega}(x') p(\omega) d\omega
\end{align}
We can obtain the kernel via 
\begin{align}
  p^*(\omega) = \argmin_{p(\omega)} & \sum_{x}\sum_{\theta} D_{\theta}x(\theta) \rbr{\int_{\Omega} D\phi_{\omega}(x(\theta)) D\phi_{\omega}(x(\theta)) p(\omega) d\omega} (D_{\theta}x(\theta))^\top + \int_{\omega} p(\omega)^2 d\omega \\
  \text{s.t.} & \int_{\omega} p(\omega) d\omega = 1, p(\omega) \geq 0  
\end{align}

\section{How to argue statistical advantage?}

A plausible strategy is to argue something like this:
\begin{itemize}
  \item The support of the useful signal is small (say sparsity level of $s$).
  \item If we do not consider eg. translation invariance, the support of of the input data is large (say an ambient dimension of $d$)
  \item Intuitively, we should show that we can get some convergence rate depending on $s$ rather than $d$.
\end{itemize}

\Zqian{
\begin{itemize}
\item
What is the role of sparsity here? If $x(\theta)$ is given, that is, we already know whose gradient should be zero.  It is not like variable selection, where we only know there exist some partial derivatives should be very small, and what we want to do is find them out. I think our problem is more like a smoothing problem, that is, we want the function values to be not so sensitive to the variation of the transformation parameter $\theta$. \Le{The smoothing interpretation is right. That's what the gradient equals to zero trying to do. The sparsity is a different interpretation (related to your next item). For instance, we are building some detector for eyes. An eye in an image (consisting of $s$ pixels) is tiny compare to the entire image, and it can be anywhere in the image. If we are able to translate the patch for the eye into some canonical position (eg. (0,0)), then the classifier only needs to focus on $s$ pixels instead of the entire image of $d$ pixels. If we ignore translation invariance, then all 
pixels in the image are relevant, since the eye can be anywhere.}  \\

\item
One thing might be possible with sparsity is that  we assume that the function depends only on a few relevant dimensions of $x(\theta)$, it follows that for any data point $x(\theta)=(x_1(\theta),\dots, x_d(\theta)), \quad \frac {\partial f}{\partial x_j (\theta)}  \cdot  \frac {\partial x_j(\theta)}{\partial \theta} = 0$ holds for any irrevelent dimension $x_j(\theta)$. \Le{Note that irrelevant dimensions are different for different $x(\theta)$. And we really want the derivatives with respect to $\theta$ to be small. Is the following sum of dimension wise $l_2$ still easy to interpret?} Thus, $\frac {\partial f}{\partial \theta}=\sum_{j=1}^d\frac {\partial f}{\partial x_j (\theta)}  \cdot  \frac {\partial x_j(\theta)}{\partial \theta}$ depends only on a few dimensions of $x(\theta)$. This variable selection problem might be done by grouping the partial derivatives in a different way, like change the penalty term above to
\begin{align*}
  \sum_{j=1}^d \rbr{\int_{\Theta} \nbr{ \frac {\partial f}{\partial x_j (\theta)}  \cdot  \frac {\partial x_j(\theta)}
  {\partial \theta} }^2 d\PP(\theta)}^{1/2}+ \tau \nbr{f}^2
\end{align*}
Estimate
$\rbr{\int_{\Theta} \nbr{ \frac {\partial f}{\partial x_j (\theta)}  \cdot  \frac {\partial x_j(\theta)}{\partial \theta} }^2 d\PP(\theta)}^{1/2}$
by
$ \frac 1m\sum_{i=1}^m \rbr{\int_{\Theta} \nbr{  \frac {\partial f}{\partial x_j^i (\theta)}  \cdot  \frac {\partial x_j^i(\theta)}{\partial \theta} }^2 d\PP(\theta)}^{1/2}$,
the penalty term can be written as
\begin{align*}
  \frac 1m\sum_{i=1}^m\sum_{j=1}^d \rbr{\int_{\Theta} \nbr{ \frac {\partial f}{\partial x_j^i (\theta)}  \cdot
  \frac {\partial x_j^i(\theta)}{\partial \theta} }^2 d\PP(\theta)}^{1/2} + \tau \nbr{f}^2
\end{align*}
That is, we move the summation along the dimensions of $x(\theta)$ outside the norm. This is like a l1/l2 regularizer, and will favor the sparsity over the dimensions of   $x(\theta)$, while $($18$)$ will not have such a sparsity property. In this way it might be possible to show that the sample complexity depends on the number of relevant dimensions s rather than d.
\\

Also, it still can control the invariance, since it is easy to see that
\begin{align*}
  \frac{1}{m} \sum_{i=1}^m \nbr{D_{\theta} f(x^i(\theta))} &=\frac{1}{m} \sum_{i=1}^m \rbr{\int_{\Theta} \nbr{D_{\theta} f(x(\theta))}^2 d\PP(\theta)}^{1/2}\\
  &=
  \frac{1}{m} \sum_{i=1}^m \rbr{\int_{\Theta} \nbr{\sum_{j=1}^d \frac {\partial f}{\partial x_j^i (\theta)}  \cdot  \frac {\partial x_j^i(\theta)}{\partial \theta} }^2 \PP(\theta)}^{1/2}\\
  &\leq
  \frac 1m\sum_{i=1}^m\sum_{j=1}^d \rbr{\int_{\Theta} \nbr{ \frac {\partial f}{\partial x_j^i (\theta)}  \cdot  \frac {\partial x_j^i(\theta)}{\partial \theta} }^2 d\PP(\theta)}^{1/2}
\end{align*}
(This is just a thought about sparsity, it still need to be verified and there might be better formulation.)\\

\item
Another thought is that under some condition, we can treat $f (x_1(\theta) \dots x_d(\theta))$ as a function of $z$ and $\theta$: $f\cdot x(z_1, \dots, z_d, \theta_1, \dots, \theta_l)$. This function should be irrelevant with $\theta$, and depend on a few $z_j$'s.  Observing that in many transformation $\frac{\partial f}{\partial z_i}$ does not depend on $z_i$. For example, in transformation, $\frac{\partial f}{\partial z_i} = \frac{\partial f}{\partial x_i} $, in 2d rotation,  $\frac{\partial x}{\partial z_i}= \frac{\partial f}{\partial x_1} \cos(\theta) +\frac{\partial f}{\partial x_2} (- \sin(\theta))$. \Le{I don't understand the notation $f\cdot x(z_1, \dots, z_d, \theta_1, \dots, \theta_l)$ and $\frac{\partial x}{\partial z_i}$. See my clarified image example in section 2.} In this sense, it is constrain $\frac{\partial f}{\partial z_i}$ might be somehow equivalent to constrain $\frac{\partial f}{\partial x_i(\theta)}$, In the spirit of variable selection, can we control the invariance by penalizing $\
sum_{j=1}^d \nbr{\frac{\partial f}{\partial x_j(\theta)} } + \sum_{i=1}^l \nbr{\frac{\partial f}{\partial \theta_i}}$?   And this regularizer might be able to enforce $\nbr{\frac{\partial f}{\partial \theta_i}}$ and some $\nbr{\frac{\partial f}{\partial x(\theta)}}$ to zero?
\end{itemize}
}


\section{Niao}

Sorry for the delay. I was trying to check in some comments in the text, but my git somehow stops working today.  

— I am not sure it is a good idea to move the invariance condition as constraints in order to use conditional gradient, in either case, it is not going to be easy because the domain induced by this condition does not seem to allow easy-to-compute linear minimization oracles. Even if it is the case, it won’t matter where we put this norm condition, since the conditional gradient algorithm still applies (you may refer to this paper, conditional gradient algorithm for norm-regularized smooth convex problems. )

Here are some more detailed comments. 



If we consider the  empirical model, this is essentially to minimize
\begin{align}
  \frac{1}{m} \sum_{i=1}^m \ell(f(x^i), y^i) + \lambda  \frac{1}{m} \sum_{i=1}^m\max_{p(\theta)\in \PP}\sum_{\theta\in\Theta}\underbrace{\rbr{p(\theta)\sum_{j=1}^d | \langle D_\theta x_j(\theta)D_{x_j(\theta)} k(x(\theta),\cdot)\, , \,f\rangle | ^q}^{1/q}}_{:=\|D_\theta f(x(\theta))\|_q}+ \mu \nbr{f}^2
\end{align}
\begin{itemize}
\item if set $\PP$ is singleton given by some known distribution $P(\theta)$, this recovers the $\ell_2$ norm as above;
\item if set $\PP$ is simplex, this recovers the $\ell_\infty$ norm as above;
\item when $q=1$, this may impose sparsity of $\beta_{ij\theta}$ w.r.t. index $j$; when $p(\theta)$ is uniform, this may impose sparsity w.r.t. index $\theta$; 
\end{itemize}

This can be solved by gradient descent directly, perhaps also via proximal algorithms, (since we can simply replace the linear forms with new variables and end up with the usual Euclidean $\|\cdot\|_q$ norms.) But I don't think moving the regularization to the constraints and using conditional gradient method will make the problem any easier, since the domain induced by this complex norm does not seem to provide easy-to-compute linear minimization oracles.\\

If we consider the stochastic setting with priorly given $P(\theta)$,  this is essentially to solve the problem
\begin{align}
  \min_f \left\{  \EE_{x,y}\ell(f(x),y)+ \lambda  \EE_{x}\EE_{\theta\sim P}\|D_\theta f(x(\theta))\|_q+ \mu \nbr{f}^2\right\}
\end{align}
This can be solved by stochastic functional gradient, in the same fashion as our doubly SGD but sampling first data point and then the invariant parameter. Analysis of the convergence immediately follows since the updates stay in the RKHS space during the process. 




Best wishes,
Niao

\section{Experiments}

\begin{itemize}
  \item One dimensional binary classification (translation and scaling invariance).
  \item Two dimensional binary classification (translation, rotation and scaling invariance).
  \item MNIST (limited translation, rotation and scaling invariance).
  \item Molecularspace (rotation invariance).
\end{itemize}

\BoX{**For future work** Besides increasing simple invariances, a hierarchical structure seems to achieve more complex ``compositional'' invariance. Consider recognizing a human, which consists of the head, the torso, and the limbs. There are great variabilities in how the positions of the parts relate to each other. In a hierarchical structure, lower levels achieves small translation invariances for each individual part. The upper levels can then build up on these parts to recognize a human more easily, since most inter-part translations are already handled in lower levels. \Le{How do you actually do it hierarchically?}}

% Simple translation-invariant concepts are hard to learn
% dl.acm.org/citation.cfm?id=185118
% Association for Computing Machinery
% by M Jerrum - ‎1994
% 
% invariant pattern recognition by sdp
% Thore and Ralf
% 
% Unifying Framework for Invariant Pattern Recognition
% wood and taylor
% 
% Permutation Invariant SVMs
% Jebara

% Wen-Jun Shen, Hau-San Wong, Quan-Wu Xiao, Xin Guo, and Stephen Smale. “Towards a
% Mathematical Foundation of Immunology and Amino Acid Chains”, arXiv:1205.6031,
% June 25, 2012.
% [18] S. Smale, L. Rosasco, J. Bouvrie, A. Caponnetto, and T. Poggio. “Mathematics of the Neural
% Response”, Foundations of ComputationalMathematics, Vol. 10(1), pp.67–91, Feb 2010.
% 
% Hierarchical Kernel Machines:
% The Mathematics of Learning Inspired by Visual CortexTitle, i.e,
% Synthetic Aperture Ladar for Tactical Imaging
% 
% J. Bouvrie, L. Rosasco, and T. Poggio. “On Invariance in Hierarchical Models”. Advances in
% Neural Information Processing Systems (NIPS) 22, 2009.
% 
% G.D. Canas, T. Poggio, L. Rosasco. “Learning Manifolds with K-Means and K-Flats”, Advances
% in Neural Information Processing Systems (NIPS) 25, 2012.
% [5] G.D. Canas, L. Rosasco. “Learning Probability Measures with respect to Optimal Transport
% Metrics”, Advances in Neural Information Processing Systems (NIPS) 25, 2012.
%
%Learning to see like children: proof of concept
% Marco Gori, Marco Lippi, Marco Maggini, Stefano Melacci
% Department of Information Engineering and Mathematics
% University of Siena, Italy
% 
% Hierarchical kernel-based rotation and scale invariant similarity
% Y.Y. Tanga, b, Tian Xiab, Yantao Weic, , , Hong Lid, Luoqing Lie

\bibliographystyle{unsrt}
\bibliography{../../../bibfile2013/bibfile}
\end{document}
