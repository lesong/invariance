import sys
import h5py
import numpy as np
import os
from random import shuffle
import scipy.io 

if __name__ == '__main__':

    dict = scipy.io.loadmat('sequence_data.mat')
    feat_size = 6000

    data = dict['sequence_mat'].astype(float)
    data = data.T
    num_samples = data.shape[0]
   
    #os.remove('markovnet_train.h5')
    #os.remove('markovnet_test.h5')

    labels = np.ones((num_samples, 1))
    labels[num_samples / 2 :, :] = 0

    p = np.random.permutation(num_samples)

    num_train = int(num_samples * 0.9)
    num_test = num_samples - num_train

    train_data = data[p[0 : num_train], :].reshape((num_train, 1, 1, feat_size))
    train_label = labels[p[0 : num_train]]
    test_data = data[p[num_train:], :].reshape((num_test, 1, 1, feat_size))
    test_label = labels[p[num_train:]]

    f_train = h5py.File('markovnet_train.h5')
    f_train['data'] = train_data
    f_train['label'] = train_label

    f_train.close()

    f_test = h5py.File('markovnet_test.h5')
    f_test['data'] = test_data
    f_test['label'] = test_label

    f_test.close()

    print len(train_label), len(test_label)
